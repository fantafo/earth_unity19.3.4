﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Invoker : MonoBehaviour
{
    private static Queue<Action> actions = new Queue<Action>();
    private static Thread mainThread;
    private static Invoker main;

    [RuntimeInitializeOnLoadMethod]
    static void Initialize()
    {
#if UNITY_EDITOR
        if (Application.isPlaying)
#endif
        {
            GameObject go = new GameObject("Invoker", typeof(Invoker));
            main = go.GetComponent<Invoker>();
            mainThread = Thread.CurrentThread;
            DontDestroyOnLoad(go);
        }
    }

    public static bool IsMainThread { get { return mainThread == Thread.CurrentThread; } }
    public static void Invoke(Action action)
    {
        if (IsMainThread)
        {
            action();
        }
        else
        {
            lock (actions)
            {
                actions.Enqueue(action);
            }
        }
    }

    private void Update()
    {
        lock (actions)
        {
            while (actions.Count > 0)
            {
                actions.Dequeue()();
            }
        }
    }
}