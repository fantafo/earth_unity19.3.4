﻿//#undef UNITY_EDITOR
using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using System.IO;

public static class SLog
{
    private static SLogger _default;
    public static SLogger Default { get { return _default ?? (_default = new SLogger("SLog")); } }

    public static SLogger GetLogger(object tag)
    {
        if (tag is UnityEngine.Object)
            return new SLogger(((UnityEngine.Object)tag).name);
        else if (tag is string)
            return new SLogger((string)tag);
        else if (tag is Type)
            return new SLogger((Type)tag);
        else
            return new SLogger(tag);
    }


    static string ToTag(object tag) { return tag != null ? tag.ToString() : Default.tag; }
    static string ToMsg(object msg) { return msg != null ? msg.ToString() : " "; }

    public static void Debug(object message) { Default.Debug(ToTag(null), ToMsg(message)); }
    public static void Debug(object tag, object message) { Default.Debug(ToTag(tag), ToMsg(message)); }
    public static void DebugFormat(object tag, string message, params object[] args) { Default.DebugFormat(ToTag(tag), ToMsg(message), args); }

    public static void Warn(object message) { Default.Warn(ToTag(null), ToMsg(message)); }
    public static void Warn(object tag, object message) { Default.Warn(ToTag(tag), ToMsg(message)); }
    public static void WarnFormat(object tag, string message, params object[] args) { Default.WarnFormat(ToTag(tag), ToMsg(message), args); }

    public static void Error(object message) { Default.Error(ToTag(null), ToMsg(message)); }
    public static void Error(object tag, object message) { Default.Error(ToTag(tag), ToMsg(message)); }
    public static void ErrorFormat(object tag, string message, params object[] args) { Default.ErrorFormat(ToTag(tag), ToMsg(message), args); }

    public static void Exception(Exception ex) { Default.Exception(ToTag(null), ex); }
    public static void Exception(object tag, Exception ex) { Default.Exception(ToTag(tag), ex); }

    public static void PrintStackTrace(this Exception e)
    {
        Exception(e);
    }

}