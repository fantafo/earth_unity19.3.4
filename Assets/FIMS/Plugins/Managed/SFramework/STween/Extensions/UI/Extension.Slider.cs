﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenSliderExtension
{
    private static readonly Action<Slider, float> _changeSliderMaxValue = __changeSliderMaxValue;
    private static void __changeSliderMaxValue(Slider slider, float val) { slider.maxValue = val; }
    public static STweenState twnMaxValue(this Slider slider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, slider.maxValue, to, duration, _changeSliderMaxValue);
    }
    public static STweenState twnMaxValue(this Slider slider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, from, to, duration, _changeSliderMaxValue);
    }


    private static readonly Action<Slider, float> _changeSliderMinValue = __changeSliderMinValue;
    private static void __changeSliderMinValue(Slider slider, float val) { slider.minValue = val; }
    public static STweenState twnMinValue(this Slider slider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, slider.minValue, to, duration, _changeSliderMinValue);
    }
    public static STweenState twnMinValue(this Slider slider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, from, to, duration, _changeSliderMinValue);
    }


    private static readonly Action<Slider, float> _changeSliderNormalizedValue = __changeSliderNormalizedValue;
    private static void __changeSliderNormalizedValue(Slider slider, float val) { slider.normalizedValue = val; }
    public static STweenState twnNormalizedValue(this Slider slider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, slider.normalizedValue, to, duration, _changeSliderNormalizedValue);
    }
    public static STweenState twnNormalizedValue(this Slider slider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, from, to, duration, _changeSliderNormalizedValue);
    }


    private static readonly Action<Slider, float> _changeSliderValue = __changeSliderValue;
    private static void __changeSliderValue(Slider slider, float val) { slider.value = val; }
    public static STweenState twnValue(this Slider slider, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, slider.value, to, duration, _changeSliderValue);
    }
    public static STweenState twnValue(this Slider slider, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Slider>, Slider, float>(slider, from, to, duration, _changeSliderValue);
    }
}
#endif