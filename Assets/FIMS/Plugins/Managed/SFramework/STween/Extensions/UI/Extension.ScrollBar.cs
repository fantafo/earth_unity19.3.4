﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenScrollBarExtension
{
    private static readonly Action<Scrollbar, float> _changeScrollbarSize = __changeScrollbarSize;
    private static void __changeScrollbarSize(Scrollbar scrollbar, float val) { scrollbar.size = val; }
    public static STweenState twnSize(this Scrollbar scrollbar, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Scrollbar>, Scrollbar, float>(scrollbar, scrollbar.size, to, duration, _changeScrollbarSize);
    }
    public static STweenState twnSize(this Scrollbar scrollbar, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Scrollbar>, Scrollbar, float>(scrollbar, from, to, duration, _changeScrollbarSize);
    }


    private static readonly Action<Scrollbar, float> _changeScrollbarValue = __changeScrollbarValue;
    private static void __changeScrollbarValue(Scrollbar scrollbar, float val) { scrollbar.value = val; }
    public static STweenState twnValue(this Scrollbar scrollbar, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Scrollbar>, Scrollbar, float>(scrollbar, scrollbar.value, to, duration, _changeScrollbarValue);
    }
    public static STweenState twnValue(this Scrollbar scrollbar, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Scrollbar>, Scrollbar, float>(scrollbar, from, to, duration, _changeScrollbarValue);
    }


    private static readonly Action<Scrollbar, int> _changeScrollbarNumberOfSteps = __changeScrollbarNumberOfSteps;
    private static void __changeScrollbarNumberOfSteps(Scrollbar scrollbar, int val) { scrollbar.numberOfSteps = val; }
    public static STweenState twnNumberOfSteps(this Scrollbar scrollbar, int to, int duration)
    {
        return STween.Play<ActionIntObject<Scrollbar>, Scrollbar, int>(scrollbar, scrollbar.numberOfSteps, to, duration, _changeScrollbarNumberOfSteps);
    }
    public static STweenState twnNumberOfSteps(this Scrollbar scrollbar, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Scrollbar>, Scrollbar, int>(scrollbar, from, to, duration, _changeScrollbarNumberOfSteps);
    }

}
#endif