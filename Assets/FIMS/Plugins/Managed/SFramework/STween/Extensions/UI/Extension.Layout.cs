﻿#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenLayoutExtension
{

    private static readonly Action<HorizontalOrVerticalLayoutGroup, float> _changeHorizontalOrVerticalLayoutGroupSpacing = __changeHorizontalOrVerticalLayoutGroupSpacing;
    private static void __changeHorizontalOrVerticalLayoutGroupSpacing(HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, float val) { horizontalOrVerticalLayoutGroup.spacing = val; }
    public static STweenState twnSpacing(this HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, float to, float duration)
    {
        return STween.Play<ActionFloatObject<HorizontalOrVerticalLayoutGroup>, HorizontalOrVerticalLayoutGroup, float>(horizontalOrVerticalLayoutGroup, horizontalOrVerticalLayoutGroup.spacing, to, duration, _changeHorizontalOrVerticalLayoutGroupSpacing);
    }
    public static STweenState twnSpacing(this HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<HorizontalOrVerticalLayoutGroup>, HorizontalOrVerticalLayoutGroup, float>(horizontalOrVerticalLayoutGroup, from, to, duration, _changeHorizontalOrVerticalLayoutGroupSpacing);
    }


    private static readonly Action<HorizontalOrVerticalLayoutGroup, RectOffset> _changeHorizontalOrVerticalLayoutGroupPadding = __changeHorizontalOrVerticalLayoutGroupPadding;
    private static void __changeHorizontalOrVerticalLayoutGroupPadding(HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, RectOffset val) { horizontalOrVerticalLayoutGroup.padding = val; }
    public static STweenState twnPadding(this HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, RectOffset to, float duration)
    {
        return STween.Play<ActionRectOffsetObject<HorizontalOrVerticalLayoutGroup>, HorizontalOrVerticalLayoutGroup, RectOffset>(horizontalOrVerticalLayoutGroup, horizontalOrVerticalLayoutGroup.padding, to, duration, _changeHorizontalOrVerticalLayoutGroupPadding);
    }
    public static STweenState twnPadding(this HorizontalOrVerticalLayoutGroup horizontalOrVerticalLayoutGroup, RectOffset from, RectOffset to, float duration)
    {
        return STween.Play<ActionRectOffsetObject<HorizontalOrVerticalLayoutGroup>, HorizontalOrVerticalLayoutGroup, RectOffset>(horizontalOrVerticalLayoutGroup, from, to, duration, _changeHorizontalOrVerticalLayoutGroupPadding);
    }

}
#endif