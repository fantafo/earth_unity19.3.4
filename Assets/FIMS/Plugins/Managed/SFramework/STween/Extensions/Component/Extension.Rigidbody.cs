﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenRigidbodyExtension
{
    private static readonly Action<Rigidbody, float> _changeRigidbodyAngularDrag = __changeRigidbodyAngularDrag;
    private static void __changeRigidbodyAngularDrag(Rigidbody rigidbody, float val) { rigidbody.angularDrag = val; }
    public static STweenState twnAngularDrag(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.angularDrag, to, duration, _changeRigidbodyAngularDrag);
    }
    public static STweenState twnAngularDrag(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodyAngularDrag);
    }


    private static readonly Action<Rigidbody, float> _changeRigidbodyDrag = __changeRigidbodyDrag;
    private static void __changeRigidbodyDrag(Rigidbody rigidbody, float val) { rigidbody.drag = val; }
    public static STweenState twnDrag(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.drag, to, duration, _changeRigidbodyDrag);
    }
    public static STweenState twnDrag(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodyDrag);
    }


    private static readonly Action<Rigidbody, float> _changeRigidbodyMass = __changeRigidbodyMass;
    private static void __changeRigidbodyMass(Rigidbody rigidbody, float val) { rigidbody.mass = val; }
    public static STweenState twnMass(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.mass, to, duration, _changeRigidbodyMass);
    }
    public static STweenState twnMass(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodyMass);
    }


    private static readonly Action<Rigidbody, float> _changeRigidbodyMaxAngularVelocity = __changeRigidbodyMaxAngularVelocity;
    private static void __changeRigidbodyMaxAngularVelocity(Rigidbody rigidbody, float val) { rigidbody.maxAngularVelocity = val; }
    public static STweenState twnMaxAngularVelocity(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.maxAngularVelocity, to, duration, _changeRigidbodyMaxAngularVelocity);
    }
    public static STweenState twnMaxAngularVelocity(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodyMaxAngularVelocity);
    }


    private static readonly Action<Rigidbody, float> _changeRigidbodyMaxDepenetrationVelocity = __changeRigidbodyMaxDepenetrationVelocity;
    private static void __changeRigidbodyMaxDepenetrationVelocity(Rigidbody rigidbody, float val) { rigidbody.maxDepenetrationVelocity = val; }
    public static STweenState twnMaxDepenetrationVelocity(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.maxDepenetrationVelocity, to, duration, _changeRigidbodyMaxDepenetrationVelocity);
    }
    public static STweenState twnMaxDepenetrationVelocity(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodyMaxDepenetrationVelocity);
    }


    //private static readonly Action<Rigidbody, float> _changeRigidbodySleepAngularVelocity = __changeRigidbodySleepAngularVelocity;
    //private static void __changeRigidbodySleepAngularVelocity(Rigidbody rigidbody, float val) { rigidbody.sleepAngularVelocity = val; }
    //public static STweenState twnSleepAngularVelocity(this Rigidbody rigidbody, float to, float duration)
    //{
    //    return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.sleepAngularVelocity, to, duration, _changeRigidbodySleepAngularVelocity);
    //}
    //public static STweenState twnSleepAngularVelocity(this Rigidbody rigidbody, float from, float to, float duration)
    //{
    //    return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodySleepAngularVelocity);
    //}


    private static readonly Action<Rigidbody, float> _changeRigidbodySleepThreshold = __changeRigidbodySleepThreshold;
    private static void __changeRigidbodySleepThreshold(Rigidbody rigidbody, float val) { rigidbody.sleepThreshold = val; }
    public static STweenState twnSleepThreshold(this Rigidbody rigidbody, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.sleepThreshold, to, duration, _changeRigidbodySleepThreshold);
    }
    public static STweenState twnSleepThreshold(this Rigidbody rigidbody, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodySleepThreshold);
    }


    //private static readonly Action<Rigidbody, float> _changeRigidbodySleepVelocity = __changeRigidbodySleepVelocity;
    //private static void __changeRigidbodySleepVelocity(Rigidbody rigidbody, float val) { rigidbody.sleepVelocity = val; }
    //public static STweenState twnSleepVelocity(this Rigidbody rigidbody, float to, float duration)
    //{
    //    return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, rigidbody.sleepVelocity, to, duration, _changeRigidbodySleepVelocity);
    //}
    //public static STweenState twnSleepVelocity(this Rigidbody rigidbody, float from, float to, float duration)
    //{
    //    return STween.Play<ActionFloatObject<Rigidbody>, Rigidbody, float>(rigidbody, from, to, duration, _changeRigidbodySleepVelocity);
    //}


    private static readonly Action<Rigidbody, int> _changeRigidbodySolverIterationCount = __changeRigidbodySolverIterationCount;
    private static void __changeRigidbodySolverIterationCount(Rigidbody rigidbody, int val) { rigidbody.solverIterations = val; }
    public static STweenState twnSolverIterationCount(this Rigidbody rigidbody, int to, int duration)
    {
        return STween.Play<ActionIntObject<Rigidbody>, Rigidbody, int>(rigidbody, rigidbody.solverIterations, to, duration, _changeRigidbodySolverIterationCount);
    }
    public static STweenState twnSolverIterationCount(this Rigidbody rigidbody, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<Rigidbody>, Rigidbody, int>(rigidbody, from, to, duration, _changeRigidbodySolverIterationCount);
    }


    private static readonly Action<Rigidbody, Vector3> _changeRigidbodyAngularVelocity = __changeRigidbodyAngularVelocity;
    private static void __changeRigidbodyAngularVelocity(Rigidbody rigidbody, Vector3 val) { rigidbody.angularVelocity = val; }
    public static STweenState twnAngularVelocity(this Rigidbody rigidbody, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, rigidbody.angularVelocity, to, duration, _changeRigidbodyAngularVelocity);
    }
    public static STweenState twnAngularVelocity(this Rigidbody rigidbody, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, from, to, duration, _changeRigidbodyAngularVelocity);
    }


    private static readonly Action<Rigidbody, Vector3> _changeRigidbodyCenterOfMass = __changeRigidbodyCenterOfMass;
    private static void __changeRigidbodyCenterOfMass(Rigidbody rigidbody, Vector3 val) { rigidbody.centerOfMass = val; }
    public static STweenState twnCenterOfMass(this Rigidbody rigidbody, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, rigidbody.centerOfMass, to, duration, _changeRigidbodyCenterOfMass);
    }
    public static STweenState twnCenterOfMass(this Rigidbody rigidbody, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, from, to, duration, _changeRigidbodyCenterOfMass);
    }


    private static readonly Action<Rigidbody, Vector3> _changeRigidbodyInertiaTensor = __changeRigidbodyInertiaTensor;
    private static void __changeRigidbodyInertiaTensor(Rigidbody rigidbody, Vector3 val) { rigidbody.inertiaTensor = val; }
    public static STweenState twnInertiaTensor(this Rigidbody rigidbody, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, rigidbody.inertiaTensor, to, duration, _changeRigidbodyInertiaTensor);
    }
    public static STweenState twnInertiaTensor(this Rigidbody rigidbody, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, from, to, duration, _changeRigidbodyInertiaTensor);
    }


    private static readonly Action<Rigidbody, Vector3> _changeRigidbodyPosition = __changeRigidbodyPosition;
    private static void __changeRigidbodyPosition(Rigidbody rigidbody, Vector3 val) { rigidbody.position = val; }
    public static STweenState twnPosition(this Rigidbody rigidbody, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, rigidbody.position, to, duration, _changeRigidbodyPosition);
    }
    public static STweenState twnPosition(this Rigidbody rigidbody, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, from, to, duration, _changeRigidbodyPosition);
    }


    private static readonly Action<Rigidbody, Vector3> _changeRigidbodyVelocity = __changeRigidbodyVelocity;
    private static void __changeRigidbodyVelocity(Rigidbody rigidbody, Vector3 val) { rigidbody.velocity = val; }
    public static STweenState twnVelocity(this Rigidbody rigidbody, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, rigidbody.velocity, to, duration, _changeRigidbodyVelocity);
    }
    public static STweenState twnVelocity(this Rigidbody rigidbody, Vector3 from, Vector3 to, float duration)
    {
        return STween.Play<ActionVector3Object<Rigidbody>, Rigidbody, Vector3>(rigidbody, from, to, duration, _changeRigidbodyVelocity);
    }


    private static readonly Action<Rigidbody, Quaternion> _changeRigidbodyInertiaTensorRotation = __changeRigidbodyInertiaTensorRotation;
    private static void __changeRigidbodyInertiaTensorRotation(Rigidbody rigidbody, Quaternion val) { rigidbody.inertiaTensorRotation = val; }
    public static STweenState twnInertiaTensorRotation(this Rigidbody rigidbody, Quaternion to, float duration)
    {
        return STween.Play<ActionQuaternionObject<Rigidbody>, Rigidbody, Quaternion>(rigidbody, rigidbody.inertiaTensorRotation, to, duration, _changeRigidbodyInertiaTensorRotation);
    }
    public static STweenState twnInertiaTensorRotation(this Rigidbody rigidbody, Quaternion from, Quaternion to, float duration)
    {
        return STween.Play<ActionQuaternionObject<Rigidbody>, Rigidbody, Quaternion>(rigidbody, from, to, duration, _changeRigidbodyInertiaTensorRotation);
    }


    private static readonly Action<Rigidbody, Quaternion> _changeRigidbodyRotation = __changeRigidbodyRotation;
    private static void __changeRigidbodyRotation(Rigidbody rigidbody, Quaternion val) { rigidbody.rotation = val; }
    public static STweenState twnRotation(this Rigidbody rigidbody, Quaternion to, float duration)
    {
        return STween.Play<ActionQuaternionObject<Rigidbody>, Rigidbody, Quaternion>(rigidbody, rigidbody.rotation, to, duration, _changeRigidbodyRotation);
    }
    public static STweenState twnRotation(this Rigidbody rigidbody, Quaternion from, Quaternion to, float duration)
    {
        return STween.Play<ActionQuaternionObject<Rigidbody>, Rigidbody, Quaternion>(rigidbody, from, to, duration, _changeRigidbodyRotation);
    }



}