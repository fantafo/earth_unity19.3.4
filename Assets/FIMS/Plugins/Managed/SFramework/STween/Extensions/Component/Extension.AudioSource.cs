﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;

public static class STweenAudioSourceExtension
{
    private static readonly Action<AudioSource, float> _changeAudioSourceDopplerLevel = __changeAudioSourceDopplerLevel;
    private static void __changeAudioSourceDopplerLevel(AudioSource audioSource, float val) { audioSource.dopplerLevel = val; }
    public static STweenState twnDopplerLevel(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.dopplerLevel, to, duration, _changeAudioSourceDopplerLevel);
    }
    public static STweenState twnDopplerLevel(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceDopplerLevel);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceMaxDistance = __changeAudioSourceMaxDistance;
    private static void __changeAudioSourceMaxDistance(AudioSource audioSource, float val) { audioSource.maxDistance = val; }
    public static STweenState twnMaxDistance(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.maxDistance, to, duration, _changeAudioSourceMaxDistance);
    }
    public static STweenState twnMaxDistance(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceMaxDistance);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceMinDistance = __changeAudioSourceMinDistance;
    private static void __changeAudioSourceMinDistance(AudioSource audioSource, float val) { audioSource.minDistance = val; }
    public static STweenState twnMinDistance(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.minDistance, to, duration, _changeAudioSourceMinDistance);
    }
    public static STweenState twnMinDistance(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceMinDistance);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourcePanStereo = __changeAudioSourcePanStereo;
    private static void __changeAudioSourcePanStereo(AudioSource audioSource, float val) { audioSource.panStereo = val; }
    public static STweenState twnPanStereo(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.panStereo, to, duration, _changeAudioSourcePanStereo);
    }
    public static STweenState twnPanStereo(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourcePanStereo);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourcePitch = __changeAudioSourcePitch;
    private static void __changeAudioSourcePitch(AudioSource audioSource, float val) { audioSource.pitch = val; }
    public static STweenState twnPitch(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.pitch, to, duration, _changeAudioSourcePitch);
    }
    public static STweenState twnPitch(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourcePitch);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceReverbZoneMix = __changeAudioSourceReverbZoneMix;
    private static void __changeAudioSourceReverbZoneMix(AudioSource audioSource, float val) { audioSource.reverbZoneMix = val; }
    public static STweenState twnReverbZoneMix(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.reverbZoneMix, to, duration, _changeAudioSourceReverbZoneMix);
    }
    public static STweenState twnReverbZoneMix(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceReverbZoneMix);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceSpatialBlend = __changeAudioSourceSpatialBlend;
    private static void __changeAudioSourceSpatialBlend(AudioSource audioSource, float val) { audioSource.spatialBlend = val; }
    public static STweenState twnSpatialBlend(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.spatialBlend, to, duration, _changeAudioSourceSpatialBlend);
    }
    public static STweenState twnSpatialBlend(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceSpatialBlend);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceSpread = __changeAudioSourceSpread;
    private static void __changeAudioSourceSpread(AudioSource audioSource, float val) { audioSource.spread = val; }
    public static STweenState twnSpread(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.spread, to, duration, _changeAudioSourceSpread);
    }
    public static STweenState twnSpread(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceSpread);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceTime = __changeAudioSourceTime;
    private static void __changeAudioSourceTime(AudioSource audioSource, float val) { audioSource.time = val; }
    public static STweenState twnTime(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.time, to, duration, _changeAudioSourceTime);
    }
    public static STweenState twnTime(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceTime);
    }


    private static readonly Action<AudioSource, float> _changeAudioSourceVolume = __changeAudioSourceVolume;
    private static void __changeAudioSourceVolume(AudioSource audioSource, float val) { audioSource.volume = val; }
    public static STweenState twnVolume(this AudioSource audioSource, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, audioSource.volume, to, duration, _changeAudioSourceVolume);
    }
    public static STweenState twnVolume(this AudioSource audioSource, float from, float to, float duration)
    {
        return STween.Play<ActionFloatObject<AudioSource>, AudioSource, float>(audioSource, from, to, duration, _changeAudioSourceVolume);
    }




    private static readonly Action<AudioSource, int> _changeAudioSourcePriority = __changeAudioSourcePriority;
    private static void __changeAudioSourcePriority(AudioSource audioSource, int val) { audioSource.priority = val; }
    public static STweenState twnPriority(this AudioSource audioSource, int to, int duration)
    {
        return STween.Play<ActionIntObject<AudioSource>, AudioSource, int>(audioSource, audioSource.priority, to, duration, _changeAudioSourcePriority);
    }
    public static STweenState twnPriority(this AudioSource audioSource, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<AudioSource>, AudioSource, int>(audioSource, from, to, duration, _changeAudioSourcePriority);
    }


    private static readonly Action<AudioSource, int> _changeAudioSourceTimeSamples = __changeAudioSourceTimeSamples;
    private static void __changeAudioSourceTimeSamples(AudioSource audioSource, int val) { audioSource.timeSamples = val; }
    public static STweenState twnTimeSamples(this AudioSource audioSource, int to, int duration)
    {
        return STween.Play<ActionIntObject<AudioSource>, AudioSource, int>(audioSource, audioSource.timeSamples, to, duration, _changeAudioSourceTimeSamples);
    }
    public static STweenState twnTimeSamples(this AudioSource audioSource, int from, int to, int duration)
    {
        return STween.Play<ActionIntObject<AudioSource>, AudioSource, int>(audioSource, from, to, duration, _changeAudioSourceTimeSamples);
    }


}