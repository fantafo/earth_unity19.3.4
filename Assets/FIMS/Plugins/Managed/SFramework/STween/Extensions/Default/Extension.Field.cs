﻿using System;
using UnityEngine;
using UnityEngine.UI;
using SFramework.TweenAction;
using System.Reflection;

public static class STweenFieldExtension
{


    private static readonly Action<Tuple<FieldInfo, object>, float> _changeFloatField = __changeFloatField;
    private static void __changeFloatField(Tuple<FieldInfo, object> obj, float val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, float> _changeFloatProp = __changeFloatProp;
    private static void __changeFloatProp(Tuple<PropertyInfo, object> obj, float val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnFloatField(this object obj, string name, float to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionFloatObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, float>(new Tuple<FieldInfo, object>(f, obj), (float)f.GetValue(obj), to, duration, _changeFloatField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionFloatObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, float>(new Tuple<PropertyInfo, object>(p, obj), (float)p.GetValue(obj, null), to, duration, _changeFloatProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnFloatField(this object obj, string name, float from, float to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionFloatObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, float>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeFloatField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionFloatObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, float>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeFloatProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }





    private static readonly Action<Tuple<FieldInfo, object>, int> _changeIntField = __changeIntField;
    private static void __changeIntField(Tuple<FieldInfo, object> obj, int val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, int> _changeIntProp = __changeIntProp;
    private static void __changeIntProp(Tuple<PropertyInfo, object> obj, int val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnIntField(this object obj, string name, int to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionIntObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, int>(new Tuple<FieldInfo, object>(f, obj), (int)f.GetValue(obj), to, duration, _changeIntField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionIntObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, int>(new Tuple<PropertyInfo, object>(p, obj), (int)p.GetValue(obj, null), to, duration, _changeIntProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnIntField(this object obj, string name, int from, int to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionIntObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, int>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeIntField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionIntObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, int>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeIntProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }



    private static readonly Action<Tuple<FieldInfo, object>, Vector2> _changeVector2Field = __changeVector2Field;
    private static void __changeVector2Field(Tuple<FieldInfo, object> obj, Vector2 val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Vector2> _changeVector2Prop = __changeVector2Prop;
    private static void __changeVector2Prop(Tuple<PropertyInfo, object> obj, Vector2 val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnVector2Field(this object obj, string name, Vector2 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector2Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector2>(new Tuple<FieldInfo, object>(f, obj), (Vector2)f.GetValue(obj), to, duration, _changeVector2Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector2Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector2>(new Tuple<PropertyInfo, object>(p, obj), (Vector2)p.GetValue(obj, null), to, duration, _changeVector2Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnVector2Field(this object obj, string name, Vector2 from, Vector2 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector2Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector2>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeVector2Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector2Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector2>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeVector2Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Vector3> _changeVector3Field = __changeVector3Field;
    private static void __changeVector3Field(Tuple<FieldInfo, object> obj, Vector3 val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Vector3> _changeVector3Prop = __changeVector3Prop;
    private static void __changeVector3Prop(Tuple<PropertyInfo, object> obj, Vector3 val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnVector3Field(this object obj, string name, Vector3 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector3Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector3>(new Tuple<FieldInfo, object>(f, obj), (Vector3)f.GetValue(obj), to, duration, _changeVector3Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector3Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector3>(new Tuple<PropertyInfo, object>(p, obj), (Vector3)p.GetValue(obj, null), to, duration, _changeVector3Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnVector3Field(this object obj, string name, Vector3 from, Vector3 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector3Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector3>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeVector3Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector3Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector3>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeVector3Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Vector4> _changeVector4Field = __changeVector4Field;
    private static void __changeVector4Field(Tuple<FieldInfo, object> obj, Vector4 val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Vector4> _changeVector4Prop = __changeVector4Prop;
    private static void __changeVector4Prop(Tuple<PropertyInfo, object> obj, Vector4 val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnVector4Field(this object obj, string name, Vector4 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector4Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector4>(new Tuple<FieldInfo, object>(f, obj), (Vector4)f.GetValue(obj), to, duration, _changeVector4Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector4Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector4>(new Tuple<PropertyInfo, object>(p, obj), (Vector4)p.GetValue(obj, null), to, duration, _changeVector4Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnVector4Field(this object obj, string name, Vector4 from, Vector4 to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionVector4Object<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Vector4>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeVector4Field);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionVector4Object<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Vector4>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeVector4Prop);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Quaternion> _changeQuaternionField = __changeQuaternionField;
    private static void __changeQuaternionField(Tuple<FieldInfo, object> obj, Quaternion val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Quaternion> _changeQuaternionProp = __changeQuaternionProp;
    private static void __changeQuaternionProp(Tuple<PropertyInfo, object> obj, Quaternion val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnQuaternionField(this object obj, string name, Quaternion to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionQuaternionObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Quaternion>(new Tuple<FieldInfo, object>(f, obj), (Quaternion)f.GetValue(obj), to, duration, _changeQuaternionField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionQuaternionObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Quaternion>(new Tuple<PropertyInfo, object>(p, obj), (Quaternion)p.GetValue(obj, null), to, duration, _changeQuaternionProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnQuaternionField(this object obj, string name, Quaternion from, Quaternion to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionQuaternionObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Quaternion>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeQuaternionField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionQuaternionObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Quaternion>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeQuaternionProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Color> _changeColorField = __changeColorField;
    private static void __changeColorField(Tuple<FieldInfo, object> obj, Color val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Color> _changeColorProp = __changeColorProp;
    private static void __changeColorProp(Tuple<PropertyInfo, object> obj, Color val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnColorField(this object obj, string name, Color to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionColorObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Color>(new Tuple<FieldInfo, object>(f, obj), (Color)f.GetValue(obj), to, duration, _changeColorField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionColorObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Color>(new Tuple<PropertyInfo, object>(p, obj), (Color)p.GetValue(obj, null), to, duration, _changeColorProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnColorField(this object obj, string name, Color from, Color to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionColorObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Color>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeColorField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionColorObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Color>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeColorProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Bounds> _changeBoundsField = __changeBoundsField;
    private static void __changeBoundsField(Tuple<FieldInfo, object> obj, Bounds val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Bounds> _changeBoundsProp = __changeBoundsProp;
    private static void __changeBoundsProp(Tuple<PropertyInfo, object> obj, Bounds val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnBoundsField(this object obj, string name, Bounds to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionBoundsObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Bounds>(new Tuple<FieldInfo, object>(f, obj), (Bounds)f.GetValue(obj), to, duration, _changeBoundsField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionBoundsObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Bounds>(new Tuple<PropertyInfo, object>(p, obj), (Bounds)p.GetValue(obj, null), to, duration, _changeBoundsProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnBoundsField(this object obj, string name, Bounds from, Bounds to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionBoundsObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Bounds>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeBoundsField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionBoundsObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Bounds>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeBoundsProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, Rect> _changeRectField = __changeRectField;
    private static void __changeRectField(Tuple<FieldInfo, object> obj, Rect val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, Rect> _changeRectProp = __changeRectProp;
    private static void __changeRectProp(Tuple<PropertyInfo, object> obj, Rect val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnRectField(this object obj, string name, Rect to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionRectObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Rect>(new Tuple<FieldInfo, object>(f, obj), (Rect)f.GetValue(obj), to, duration, _changeRectField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionRectObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Rect>(new Tuple<PropertyInfo, object>(p, obj), (Rect)p.GetValue(obj, null), to, duration, _changeRectProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnRectField(this object obj, string name, Rect from, Rect to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionRectObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, Rect>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeRectField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionRectObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, Rect>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeRectProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

    private static readonly Action<Tuple<FieldInfo, object>, RectOffset> _changeRectOffsetField = __changeRectOffsetField;
    private static void __changeRectOffsetField(Tuple<FieldInfo, object> obj, RectOffset val) { obj.a.SetValue(obj.b, val); }

    private static readonly Action<Tuple<PropertyInfo, object>, RectOffset> _changeRectOffsetProp = __changeRectOffsetProp;
    private static void __changeRectOffsetProp(Tuple<PropertyInfo, object> obj, RectOffset val) { obj.a.SetValue(obj.b, val, null); }

    public static STweenState twnRectOffsetField(this object obj, string name, RectOffset to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionRectOffsetObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, RectOffset>(new Tuple<FieldInfo, object>(f, obj), (RectOffset)f.GetValue(obj), to, duration, _changeRectOffsetField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionRectOffsetObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, RectOffset>(new Tuple<PropertyInfo, object>(p, obj), (RectOffset)p.GetValue(obj, null), to, duration, _changeRectOffsetProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }
    public static STweenState twnRectOffsetField(this object obj, string name, RectOffset from, RectOffset to, float duration)
    {
        var type = obj.GetType();
        FieldInfo f = type.GetField(name);
        if (f != null)
        {
            return STween.Play<ActionRectOffsetObject<Tuple<FieldInfo, object>>, Tuple<FieldInfo, object>, RectOffset>(new Tuple<FieldInfo, object>(f, obj), from, to, duration, _changeRectOffsetField);
        }
        else
        {
            PropertyInfo p = type.GetProperty(name);
            if (p != null)
            {
                return STween.Play<ActionRectOffsetObject<Tuple<PropertyInfo, object>>, Tuple<PropertyInfo, object>, RectOffset>(new Tuple<PropertyInfo, object>(p, obj), from, to, duration, _changeRectOffsetProp);
            }
            else
            {
                throw new EntryPointNotFoundException(type.Name + " is not contains '" + name + "'");
            }
        }
    }

}