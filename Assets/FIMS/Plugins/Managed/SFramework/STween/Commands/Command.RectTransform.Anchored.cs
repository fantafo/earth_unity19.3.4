﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    #region only To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                        Anchored Position 3D                 //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState anchored(Component comp, Vector3 to, float duration)
    {
        return anchored((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchored(GameObject go, Vector3 to, float duration)
    {
        return anchored((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchored(Transform trans, Vector3 to, float duration)
    {
        return anchored(trans as RectTransform, to, duration);
    }
    public static STweenState anchored(RectTransform trans, Vector3 to, float duration)
    {
        return Play<ActionAnchored, RectTransform, Vector3>(trans, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                     Anchored Position Single                //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState anchoredX(Component comp, float to, float duration)
    {
        return anchoredX((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredX(GameObject go, float to, float duration)
    {
        return anchoredX((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredX(Transform trans, float to, float duration)
    {
        return anchoredX(trans as RectTransform, to, duration);
    }
    public static STweenState anchoredX(RectTransform trans, float to, float duration)
    {
        return Play<ActionAnchoredX, RectTransform, Vector3>(trans, to, duration);
    }

    public static STweenState anchoredY(Component comp, float to, float duration)
    {
        return anchoredY((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredY(GameObject go, float to, float duration)
    {
        return anchoredY((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredY(Transform trans, float to, float duration)
    {
        return anchoredY(trans as RectTransform, to, duration);
    }
    public static STweenState anchoredY(RectTransform trans, float to, float duration)
    {
        return Play<ActionAnchoredY, RectTransform, Vector3>(trans, to, duration);
    }

    public static STweenState anchoredZ(Component comp, float to, float duration)
    {
        return anchoredZ((comp != null ? comp.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredZ(GameObject go, float to, float duration)
    {
        return anchoredZ((go != null ? go.transform : null) as RectTransform, to, duration);
    }
    public static STweenState anchoredZ(Transform trans, float to, float duration)
    {
        return anchoredZ(trans as RectTransform, to, duration);
    }
    public static STweenState anchoredZ(RectTransform trans, float to, float duration)
    {
        return Play<ActionAnchoredZ, RectTransform, Vector3>(trans, to, duration);
    }
    #endregion

    #region From, To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                        Anchored Position 3D                 //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState anchored(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return anchored((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchored(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return anchored((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchored(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return anchored(trans as RectTransform, from, to, duration);
    }
    public static STweenState anchored(RectTransform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionAnchored, RectTransform, Vector3>(trans, from, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                     Anchored Position Single                //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState anchoredX(Component comp, float from, float to, float duration)
    {
        return anchoredX((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredX(GameObject go, float from, float to, float duration)
    {
        return anchoredX((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredX(Transform trans, float from, float to, float duration)
    {
        return anchoredX(trans as RectTransform, from, to, duration);
    }
    public static STweenState anchoredX(RectTransform trans, float from, float to, float duration)
    {
        return Play<ActionAnchoredX, RectTransform, Vector3>(trans, from, to, duration);
    }

    public static STweenState anchoredY(Component comp, float from, float to, float duration)
    {
        return anchoredY((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredY(GameObject go, float from, float to, float duration)
    {
        return anchoredY((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredY(Transform trans, float from, float to, float duration)
    {
        return anchoredY(trans as RectTransform, from, to, duration);
    }
    public static STweenState anchoredY(RectTransform trans, float from, float to, float duration)
    {
        return Play<ActionAnchoredY, RectTransform, Vector3>(trans, from, to, duration);
    }

    public static STweenState anchoredZ(Component comp, float from, float to, float duration)
    {
        return anchoredZ((comp != null ? comp.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredZ(GameObject go, float from, float to, float duration)
    {
        return anchoredZ((go != null ? go.transform : null) as RectTransform, from, to, duration);
    }
    public static STweenState anchoredZ(Transform trans, float from, float to, float duration)
    {
        return anchoredZ(trans as RectTransform, from, to, duration);
    }
    public static STweenState anchoredZ(RectTransform trans, float from, float to, float duration)
    {
        return Play<ActionAnchoredZ, RectTransform, Vector3>(trans, from, to, duration);
    }
    #endregion
}
#endif