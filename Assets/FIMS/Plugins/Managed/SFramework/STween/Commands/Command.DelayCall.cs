﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    public static STweenState delayCall(float duration, Action onComplete = null)
    {
        var state = PlayBase<ActionDelayCall>(duration);
        state.SetOnComplete(onComplete);
        return state;
    }
    public static STweenState delayUpdate(float duration, Action onUpdate = null)
    {
        var state = PlayBase<ActionDelayCall>(duration);
        state.SetOnUpdate(onUpdate);
        return state;
    }
    public static STweenState delayUpdate(float duration, Func<bool> onUpdate = null)
    {
        var state = PlayBase<ActionDelayCall>(duration);
        state.SetOnUpdate(onUpdate);
        return state;
    }
    public static STweenState loop(Func<bool> onUpdate = null)
    {
        var state = PlayBase<ActionDelayCall>(float.MaxValue);
        state.SetOnUpdate(onUpdate);
        return state;
    }
}