﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    #region only To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Rotate                           //
    //                                                             //
    /////////////////////////////////////////////////////////////////
    public static STweenState rotateQ(Component comp, Quaternion to, float duration)
    {
        return rotateQ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateQ(GameObject go, Quaternion to, float duration)
    {
        return rotateQ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateQ(Transform trans, Quaternion to, float duration)
    {
        return Play<ActionRotateQuat, Transform, Quaternion>(trans, to, duration);
    }
    public static STweenState rotateLocalQ(Component comp, Quaternion to, float duration)
    {
        return rotateLocalQ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocalQ(GameObject go, Quaternion to, float duration)
    {
        return rotateLocalQ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocalQ(Transform trans, Quaternion to, float duration)
    {
        return Play<ActionRotateLocalQuat, Transform, Quaternion>(trans, to, duration);
    }

    public static STweenState rotateQ(Component comp, Vector3 to, float duration)
    {
        return rotateQ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateQ(GameObject go, Vector3 to, float duration)
    {
        return rotateQ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateQ(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionRotateQuat, Transform, Quaternion>(trans, Quaternion.Euler(to), duration);
    }
    public static STweenState rotateLocalQ(Component comp, Vector3 to, float duration)
    {
        return rotateLocalQ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocalQ(GameObject go, Vector3 to, float duration)
    {
        return rotateLocalQ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocalQ(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionRotateLocalQuat, Transform, Quaternion>(trans, Quaternion.Euler(to), duration);
    }


    public static STweenState rotate(Component comp, Vector3 to, float duration)
    {
        return rotate(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotate(GameObject go, Vector3 to, float duration)
    {
        return rotate(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotate(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionRotate, Transform, Vector3>(trans, to, duration);
    }
    public static STweenState rotateLocal(Component comp, Vector3 to, float duration)
    {
        return rotateLocal(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocal(GameObject go, Vector3 to, float duration)
    {
        return rotateLocal(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocal(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionRotateLocal, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotate(Component comp, Quaternion to, float duration)
    {
        return rotate(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotate(GameObject go, Quaternion to, float duration)
    {
        return rotate(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotate(Transform trans, Quaternion to, float duration)
    {
        return Play<ActionRotate, Transform, Vector3>(trans, to.eulerAngles, duration);
    }
    public static STweenState rotateLocal(Component comp, Quaternion to, float duration)
    {
        return rotateLocal(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocal(GameObject go, Quaternion to, float duration)
    {
        return rotateLocal(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocal(Transform trans, Quaternion to, float duration)
    {
        return Play<ActionRotateLocal, Transform, Vector3>(trans, to.eulerAngles, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Rotate Around                       //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState rotateAround(Component comp, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAround(comp != null ? comp.transform : null, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAround(GameObject go, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAround(go != null ? go.transform : null, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAround(Transform trans, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAround(trans, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAround(Transform trans, Tuple<Vector3, Vector3, float> to, float duration)
    {
        return Play<ActionRotateAround, Transform, Tuple<Vector3, Vector3, float>, float>
            (trans, to, duration);
    }

    public static STweenState rotateAroundLocal(Component comp, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAroundLocal(comp != null ? comp.transform : null, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAroundLocal(GameObject go, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAroundLocal(go != null ? go.transform : null, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAroundLocal(Transform trans, Vector3 point, Vector3 axis, float angle, float duration)
    {
        return rotateAroundLocal(trans, new Tuple<Vector3, Vector3, float>(point, axis, angle), duration);
    }
    public static STweenState rotateAroundLocal(Transform trans, Tuple<Vector3, Vector3, float> to, float duration)
    {
        return Play<ActionRotateAroundLocal, Transform, Tuple<Vector3, Vector3, float>, float>
            (trans, to, duration);
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                       Rotate Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState rotateX(Component comp, float to, float duration)
    {
        return rotateX(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateX(GameObject go, float to, float duration)
    {
        return rotateX(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateX(Transform trans, float to, float duration)
    {
        return Play<ActionRotateX, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotateY(Component comp, float to, float duration)
    {
        return rotateY(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateY(GameObject go, float to, float duration)
    {
        return rotateY(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateY(Transform trans, float to, float duration)
    {
        return Play<ActionRotateY, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotateZ(Component comp, float to, float duration)
    {
        return rotateZ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateZ(GameObject go, float to, float duration)
    {
        return rotateZ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateZ(Transform trans, float to, float duration)
    {
        return Play<ActionRotateZ, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotateLocalX(Component comp, float to, float duration)
    {
        return rotateLocalX(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocalX(GameObject go, float to, float duration)
    {
        return rotateLocalX(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocalX(Transform trans, float to, float duration)
    {
        return Play<ActionRotateLocalX, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotateLocalY(Component comp, float to, float duration)
    {
        return rotateLocalY(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocalY(GameObject go, float to, float duration)
    {
        return rotateLocalY(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocalY(Transform trans, float to, float duration)
    {
        return Play<ActionRotateLocalY, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState rotateLocalZ(Component comp, float to, float duration)
    {
        return rotateLocalZ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState rotateLocalZ(GameObject go, float to, float duration)
    {
        return rotateLocalZ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState rotateLocalZ(Transform trans, float to, float duration)
    {
        return Play<ActionRotateLocalZ, Transform, Vector3>(trans, to, duration);
    }
    #endregion

    #region From, To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Rotate                           //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState rotate(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return rotate(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotate(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return rotate(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotate(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionRotate, Transform, Vector3>(trans, from, to, duration);
    }
    public static STweenState rotateLocal(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return rotateLocal(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocal(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return rotateLocal(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocal(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionRotateLocal, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotate(Component comp, Quaternion from, Quaternion to, float duration)
    {
        return rotate(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotate(GameObject go, Quaternion from, Quaternion to, float duration)
    {
        return rotate(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotate(Transform trans, Quaternion from, Quaternion to, float duration)
    {
        return Play<ActionRotate, Transform, Vector3>(trans, from.eulerAngles, to.eulerAngles, duration);
    }
    public static STweenState rotateLocal(Component comp, Quaternion from, Quaternion to, float duration)
    {
        return rotateLocal(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocal(GameObject go, Quaternion from, Quaternion to, float duration)
    {
        return rotateLocal(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocal(Transform trans, Quaternion from, Quaternion to, float duration)
    {
        return Play<ActionRotateLocal, Transform, Vector3>(trans, from.eulerAngles, to.eulerAngles, duration);
    }


    public static STweenState rotateQ(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return rotateQ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateQ(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return rotateQ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateQ(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionRotateQuat, Transform, Quaternion>(trans, Quaternion.Euler(from), Quaternion.Euler(to), duration);
    }
    public static STweenState rotateLocalQ(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return rotateLocalQ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalQ(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return rotateLocalQ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalQ(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionRotateLocalQuat, Transform, Quaternion>(trans, Quaternion.Euler(from), Quaternion.Euler(to), duration);
    }

    public static STweenState rotateQ(Component comp, Quaternion from, Quaternion to, float duration)
    {
        return rotateQ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateQ(GameObject go, Quaternion from, Quaternion to, float duration)
    {
        return rotateQ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateQ(Transform trans, Quaternion from, Quaternion to, float duration)
    {
        return Play<ActionRotateQuat, Transform, Quaternion>(trans, from, to, duration);
    }
    public static STweenState rotateLocalQ(Component comp, Quaternion from, Quaternion to, float duration)
    {
        return rotateLocalQ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalQ(GameObject go, Quaternion from, Quaternion to, float duration)
    {
        return rotateLocalQ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalQ(Transform trans, Quaternion from, Quaternion to, float duration)
    {
        return Play<ActionRotateLocalQuat, Transform, Quaternion>(trans, from, to, duration);
    }



    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                       Rotate Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState rotateX(Component comp, float from, float to, float duration)
    {
        return rotateX(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateX(GameObject go, float from, float to, float duration)
    {
        return rotateX(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateX(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateX, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotateY(Component comp, float from, float to, float duration)
    {
        return rotateY(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateY(GameObject go, float from, float to, float duration)
    {
        return rotateY(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateY(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateY, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotateZ(Component comp, float from, float to, float duration)
    {
        return rotateZ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateZ(GameObject go, float from, float to, float duration)
    {
        return rotateZ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateZ(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateZ, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotateLocalX(Component comp, float from, float to, float duration)
    {
        return rotateLocalX(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalX(GameObject go, float from, float to, float duration)
    {
        return rotateLocalX(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalX(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateLocalX, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotateLocalY(Component comp, float from, float to, float duration)
    {
        return rotateLocalY(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalY(GameObject go, float from, float to, float duration)
    {
        return rotateLocalY(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalY(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateLocalY, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState rotateLocalZ(Component comp, float from, float to, float duration)
    {
        return rotateLocalZ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalZ(GameObject go, float from, float to, float duration)
    {
        return rotateLocalZ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState rotateLocalZ(Transform trans, float from, float to, float duration)
    {
        return Play<ActionRotateLocalZ, Transform, Vector3>(trans, from, to, duration);
    }
    #endregion
}