﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;
using SFramework.TweenAction;

public partial class STween
{
    #region only To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Move                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState move(Component comp, Vector3 to, float duration)
    {
        return move(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState move(GameObject go, Vector3 to, float duration)
    {
        return move(go != null ? go.transform : null, to, duration);
    }
    public static STweenState move(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionMove, Transform, Vector3>(trans, to, duration);
    }
    public static STweenState moveLocal(Component comp, Vector3 to, float duration)
    {
        return moveLocal(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveLocal(GameObject go, Vector3 to, float duration)
    {
        return moveLocal(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveLocal(Transform trans, Vector3 to, float duration)
    {
        return Play<ActionMoveLocal, Transform, Vector3>(trans, to, duration);
    }


    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Move Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState moveX(Component comp, float to, float duration)
    {
        return moveX(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveX(GameObject go, float to, float duration)
    {
        return moveX(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveX(Transform trans, float to, float duration)
    {
        return Play<ActionMoveX, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState moveY(Component comp, float to, float duration)
    {
        return moveY(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveY(GameObject go, float to, float duration)
    {
        return moveY(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveY(Transform trans, float to, float duration)
    {
        return Play<ActionMoveY, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState moveZ(Component comp, float to, float duration)
    {
        return moveZ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveZ(GameObject go, float to, float duration)
    {
        return moveZ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveZ(Transform trans, float to, float duration)
    {
        return Play<ActionMoveZ, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState moveLocalX(Component comp, float to, float duration)
    {
        return moveLocalX(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveLocalX(GameObject go, float to, float duration)
    {
        return moveLocalX(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveLocalX(Transform trans, float to, float duration)
    {
        return Play<ActionMoveLocalX, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState moveLocalY(Component comp, float to, float duration)
    {
        return moveLocalY(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveLocalY(GameObject go, float to, float duration)
    {
        return moveLocalY(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveLocalY(Transform trans, float to, float duration)
    {
        return Play<ActionMoveLocalY, Transform, Vector3>(trans, to, duration);
    }

    public static STweenState moveLocalZ(Component comp, float to, float duration)
    {
        return moveLocalZ(comp != null ? comp.transform : null, to, duration);
    }
    public static STweenState moveLocalZ(GameObject go, float to, float duration)
    {
        return moveLocalZ(go != null ? go.transform : null, to, duration);
    }
    public static STweenState moveLocalZ(Transform trans, float to, float duration)
    {
        return Play<ActionMoveLocalZ, Transform, Vector3>(trans, to, duration);
    }
    #endregion

    #region From, To
    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Move                             //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState move(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return move(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState move(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return move(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState move(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionMove, Transform, Vector3>(trans, from, to, duration);
    }
    public static STweenState moveLocal(Component comp, Vector3 from, Vector3 to, float duration)
    {
        return moveLocal(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveLocal(GameObject go, Vector3 from, Vector3 to, float duration)
    {
        return moveLocal(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveLocal(Transform trans, Vector3 from, Vector3 to, float duration)
    {
        return Play<ActionMoveLocal, Transform, Vector3>(trans, from, to, duration);
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                         Move Single                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public static STweenState moveX(Component comp, float from, float to, float duration)
    {
        return moveX(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveX(GameObject go, float from, float to, float duration)
    {
        return moveX(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveX(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveX, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState moveY(Component comp, float from, float to, float duration)
    {
        return moveY(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveY(GameObject go, float from, float to, float duration)
    {
        return moveY(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveY(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveY, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState moveZ(Component comp, float from, float to, float duration)
    {
        return moveZ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveZ(GameObject go, float from, float to, float duration)
    {
        return moveZ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveZ(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveZ, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState moveLocalX(Component comp, float from, float to, float duration)
    {
        return moveLocalX(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveLocalX(GameObject go, float from, float to, float duration)
    {
        return moveLocalX(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveLocalX(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveLocalX, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState moveLocalY(Component comp, float from, float to, float duration)
    {
        return moveLocalY(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveLocalY(GameObject go, float from, float to, float duration)
    {
        return moveLocalY(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveLocalY(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveLocalY, Transform, Vector3>(trans, from, to, duration);
    }

    public static STweenState moveLocalZ(Component comp, float from, float to, float duration)
    {
        return moveLocalZ(comp != null ? comp.transform : null, from, to, duration);
    }
    public static STweenState moveLocalZ(GameObject go, float from, float to, float duration)
    {
        return moveLocalZ(go != null ? go.transform : null, from, to, duration);
    }
    public static STweenState moveLocalZ(Transform trans, float from, float to, float duration)
    {
        return Play<ActionMoveLocalZ, Transform, Vector3>(trans, from, to, duration);
    }
    #endregion
}