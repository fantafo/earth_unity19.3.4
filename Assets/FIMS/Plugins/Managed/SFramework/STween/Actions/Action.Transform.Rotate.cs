﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;

namespace SFramework.TweenAction
{
    [Serializable]
    public class ActionRotate : ComponentAction<Transform, Vector3, Vector3>
    {
        public override void Init()
        {
            SetFrom(_current = _target.eulerAngles);
        }

        public override void CalcDifference()
        {
            diff = to - from;
        }
        public override void Update(float ratio)
        {
            if (float.IsNaN(ratio))
            {
                Debug.Log("NAN");
            }
            _current = from + (diff * ratio);
            _target.rotation = Quaternion.Euler(_current);
        }
    }
    [Serializable]
    public class ActionRotateLocal : ComponentAction<Transform, Vector3, Vector3>
    {
        public override void Init()
        {
            SetFrom(_current = _target.localEulerAngles);
        }

        public override void CalcDifference()
        {
            diff = to - from;
        }
        public override void Update(float ratio)
        {
            _current = from + (diff * ratio);
            _target.localEulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateQuat : ComponentAction<Transform, Quaternion, Quaternion>
    {
        public override void Init()
        {
            SetFrom(_current = _target.rotation);
        }
        public override void CalcDifference()
        {
            base.CalcDifference();
        }

        public override void Update(float ratio)
        {
            _current = Quaternion.Lerp(from, to, ratio);
            _target.rotation = _current;
        }
    }
    [Serializable]
    public class ActionRotateLocalQuat : ComponentAction<Transform, Quaternion, Quaternion>
    {
        public override void Init()
        {
            SetFrom(_current = _target.localRotation);
        }

        public override void Update(float ratio)
        {
            _current = Quaternion.Lerp(from, to, ratio);
            _target.localRotation = _current;
        }
    }

    /// <summary>
    /// Rotate the object around the point.
    /// Don't support apply variable From
    /// 
    /// Tuple.a : Point
    /// Tuple.b : Axis
    /// Tuple.c : Angle
    /// </summary>
    [Serializable]
    public class ActionRotateAround
        : ComponentAction<Transform, Tuple<Vector3, Vector3, float>, float>
    {
        public override void Init()
        {
            _current = 0;
        }

        public override void Update(float ratio)
        {
            float cur = to.c * ratio;
            float val = cur - _current;
            _current = cur;

            _target.RotateAround(to.a, to.b, val);
        }
    }
    /// <summary>
    /// Rotate the object around the local point.
    /// Don't support apply variable From
    /// 
    /// Tuple.a : Point
    /// Tuple.b : Axis
    /// Tuple.c : Angle
    /// </summary>
    [Serializable]
    public class ActionRotateAroundLocal
        : ComponentAction<Transform, Tuple<Vector3, Vector3, float>, float>
    {
        Transform parent;

        public override void Init()
        {
            parent = _target.parent;
            _current = 0;
        }

        public override void Update(float ratio)
        {
            Vector3 point = to.a;
            Vector3 axis = to.b;
            if (parent != null)
            {
                point += parent.position;
                axis = parent.rotation * axis;
            }

            float cur = to.c * ratio;
            float val = cur - _current;
            _current = cur;

            _target.RotateAround(point, axis, val);
        }
    }

    #region Transform 1D Rotating(X,Y,Z)
    [Serializable]
    public class ActionRotateX : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.eulerAngles;
            SetFrom(_current.x);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.x = v;
        }

        public override void Update(float ratio)
        {
            _current.x = from + (diff * ratio);
            _target.eulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateY : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.eulerAngles;
            SetFrom(_current.y);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.y = v;
        }

        public override void Update(float ratio)
        {
            _current.y = from + (diff * ratio);
            _target.eulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateZ : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.eulerAngles;
            SetFrom(_current.z);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.z = v;
        }

        public override void Update(float ratio)
        {
            _current.z = from + (diff * ratio);
            _target.eulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateLocalX : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.localEulerAngles;
            SetFrom(_current.x);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.x = v;
        }

        public override void Update(float ratio)
        {
            _current.x = from + (diff * ratio);
            _target.localEulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateLocalY : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.localEulerAngles;
            SetFrom(_current.y);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.y = v;
        }

        public override void Update(float ratio)
        {
            _current.y = from + (diff * ratio);
            _target.localEulerAngles = _current;
        }
    }
    [Serializable]
    public class ActionRotateLocalZ : ComponentActionDiffFloat<Transform, Vector3>
    {
        public override void Init()
        {
            _current = _target.localEulerAngles;
            SetFrom(_current.z);
        }

        public override void SetFrom(float v)
        {
            base.SetFrom(v);
            _current.z = v;
        }

        public override void Update(float ratio)
        {
            _current.z = from + (diff * ratio);
            _target.localEulerAngles = _current;
        }
    }
    #endregion
}