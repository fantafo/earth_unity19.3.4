﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;

namespace SFramework.TweenAction
{
    public abstract class BaseActionValueObject<C, T> : ComponentAction<Action<C, T>, T>
    {
        protected C argument;
        public override void SetArgument(object o)
        {
            argument = (C)o;
        }
        public override void Reset()
        {
            base.Reset();
            argument = default(C);
        }
    }

    public class ActionIntObject<C> : BaseActionValueObject<C, int>
    {
        int bef;
        public override void SetFrom(int v)
        {
            base.SetFrom(v);
            bef = from - 1;
        }

        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            int value = (int)(from + (diff * ratio));
            if (bef != value)
            {
                bef = value;
                _target(argument, value);
            }
        }
    }
    public class ActionFloatObject<C> : BaseActionValueObject<C, float>
    {
        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            var value = from + (diff * ratio);
            _target(argument, value);
        }
    }
    public class ActionVector2Object<C> : BaseActionValueObject<C, Vector2>
    {
        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            var value = from + (diff * ratio);
            _target(argument, value);
        }
    }
    public class ActionVector3Object<C> : BaseActionValueObject<C, Vector3>
    {
        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            var value = from + (diff * ratio);
            _target(argument, value);
        }
    }
    public class ActionVector4Object<C> : BaseActionValueObject<C, Vector4>
    {
        public override void CalcDifference() { diff = to - from; }
        public override void Update(float ratio)
        {
            var value = from + (diff * ratio);
            _target(argument, value);
        }
    }
    public class ActionQuaternionObject<C> : BaseActionValueObject<C, Quaternion>
    {
        public override void Update(float ratio)
        {
            var value = Quaternion.Lerp(from, to, ratio);
            _target(argument, value);
        }
    }
    public class ActionColorObject<C> : BaseActionValueObject<C, Color>
    {
        public override void Update(float ratio)
        {
            var value = Color.Lerp(from, to, ratio);
            _target(argument, value);
        }
    }
    public class ActionBoundsObject<C> : BaseActionValueObject<C, Bounds>
    {
        public override void Update(float ratio)
        {
            _target(argument, new Bounds(
                Vector3.Lerp(from.center, to.center, ratio),
                Vector3.Lerp(from.size, to.size, ratio)));
        }
    }
    public class ActionRectObject<C> : BaseActionValueObject<C, Rect>
    {
        public override void Update(float ratio)
        {
            _target(argument, new Rect(
                Mathf.Lerp(from.x, to.x, ratio),
                Mathf.Lerp(from.x, to.x, ratio),
                Mathf.Lerp(from.width, to.width, ratio),
                Mathf.Lerp(from.height, to.height, ratio)));
        }
    }
    public class ActionRectOffsetObject<C> : BaseActionValueObject<C, RectOffset>
    {
        public override void Update(float ratio)
        {
            _target(argument, new RectOffset(
                (int)Mathf.Lerp(from.left, to.left, ratio),
                (int)Mathf.Lerp(from.right, to.right, ratio),
                (int)Mathf.Lerp(from.top, to.top, ratio),
                (int)Mathf.Lerp(from.bottom, to.bottom, ratio)));
        }
    }
}