﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;

namespace SFramework.TweenAction
{
    [Serializable]
    public class ActionColorMaterial : ComponentAction<Material, Color, Color>
    {
        public override void Init()
        {
            SetFrom(_current = _target.color);
        }
        public override void Update(float ratio)
        {
            _current = Color.Lerp(from, to, ratio);
            _target.color = _current;
        }
    }

    [Serializable]
    public class ActionColorVertexMesh : ComponentAction<Mesh, Color, Color>
    {
        private Color32[] colors;

        public override void Init()
        {
            colors = new Color32[_target.vertexCount];
            SetFrom(_current = _target.colors32[0]);
        }
        public override void Update(float ratio)
        {
            _current = Color.Lerp(from, to, ratio);

            for (int i = 0; i < colors.Length; i++)
                colors[i] = _current;

            _target.colors32 = colors;
        }
    }

#if !UNITY_3_5 && !UNITY_4_0 && !UNITY_4_0_1 && !UNITY_4_1 && !UNITY_4_2 && !UNITY_4_3 && !UNITY_4_5
    [Serializable]
    public class ActionColorGraphic : ComponentAction<UnityEngine.UI.Graphic, Color, Color>
    {
        public override void Init()
        {
            SetFrom(_current = _target.color);
        }
        public override void Update(float ratio)
        {
            _current = Color.Lerp(from, to, ratio);
            _target.color = _current;
        }
    }
#endif
}