﻿using Sions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

using egl = UnityEditor.EditorGUILayout;
using gl = UnityEngine.GUILayout;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    public class BoneViewer : EditorWindow
    {
        [MenuItem("SF/Component/SkinnedMeshRenderer/BoneViewer")]
        public static void ShowWindow()
        {
            GetWindow<BoneViewer>();
        }

        SkinnedMeshRenderer renderer;
        Vector2 scroll;

        void OnGUI()
        {
            scroll = egl.BeginScrollView(scroll);

            renderer = (SkinnedMeshRenderer)egl.ObjectField("Renderer", renderer, typeof(SkinnedMeshRenderer), true);
            gl.Space(10);

            if (renderer != null)
            {
                var bones = renderer.bones;
                int newSize = egl.IntField("Size", bones.Length);
                if (newSize != bones.Length)
                {
                    System.Array.Resize(ref bones, newSize);
                }

                for (int i = 0; i < bones.Length; i++)
                {
                    bones[i] = (Transform)egl.ObjectField(i.ToString(), bones[i], typeof(Transform), true);
                }
            }

            egl.EndScrollView();
        }
    }
}