﻿using Sions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

/// <summary>
/// Author : 정상철
/// </summary>
namespace ExtensionEditor
{
    [InitializeOnLoad]
    public class RecoverySkinedMesh
    {
        [MenuItem("Sions/Utils/RecoverySkinedMesh", priority = -1000)]
        public static void Recovery()
        {
            var go = Selection.activeGameObject;

            foreach (var r in go.GetComponentsInChildren<SkinnedMeshRenderer>(true))
            {
                try
                {
                    string path = AssetDatabase.GetAssetPath(r.sharedMesh);

                    var asset = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                    var assetTrans = asset.transform.FindChildDeep(r.name);
                    var assetRenderer = assetTrans.GetComponent<SkinnedMeshRenderer>();

                    var bones = assetRenderer.bones;
                    for (int i = 0; i < bones.Length; i++)
                    {
                        var bone = go.transform.FindChildDeep(bones[i].name);
                        if (bone)
                        {
                            bones[i] = bone;
                        }
                        else
                        {
                            Debug.LogWarning("Not Found '" + bones[i].name + "'");
                            bones[i] = null;
                        }
                    }
                    r.bones = bones;
                    r.rootBone = assetRenderer.rootBone ? go.transform.FindChildDeep(assetRenderer.rootBone.name) : null;

                    Debug.Log("Changed '" + r.name + "'", r);
                }
                catch (Exception e)
                {
                    Debug.LogError("Exception : " + r.name);
                    throw e;
                }
            }
        }
        //[MenuItem("Sions/Utils/Revert SkinedMesh", priority = -1000)]
        //public static void RevertSkinnedMesh()
        //{
        //    var go = Selection.activeGameObject;
        //    var r = go.GetComponent<SkinnedMeshRenderer>();
        //    var bone = r.bones;
        //    for(int i=0; i<bone.Length; i++)
        //    {
        //        Debug.Log(bone[i].name);
        //    }
        //}
    }
}