﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions
{
    public class MirrorTransform
    {
        class TSF
        {
            public Vector3 posision;
            public Quaternion rottation;
            public Vector3 scale;

            public TSF(Vector3 zero, Quaternion identity, Vector3 localScale)
            {
                this.posision = zero;
                this.rottation = identity;
                this.scale = localScale;
            }
        }

        [MenuItem("GameObject/Transform/Mirror")]
        public static void Mirror()
        {
            var mirror = Selection.activeTransform;

            Dictionary<Transform, TSF> temp = new Dictionary<Transform, TSF>();
            var trs = mirror.GetComponentsInChildren<Transform>();
            foreach (var tr in trs)
            {
                if (tr == mirror)
                    continue;

                temp.Add(tr, new TSF(Vector3.zero, Quaternion.identity, tr.localScale));
            }

            mirror.localScale = new Vector3(-1, 1, 1);

            foreach (var pair in temp)
            {
                var tuple = pair.Value;
                tuple.posision = pair.Key.position;
                tuple.rottation = pair.Key.rotation;
            }

            mirror.localScale = new Vector3(1, 1, 1);

            foreach (var pair in temp)
            {
                pair.Key.position = pair.Value.posision;
                pair.Key.rotation = pair.Value.rottation;
                pair.Key.localScale = pair.Value.scale;
            }

        }
    }
}