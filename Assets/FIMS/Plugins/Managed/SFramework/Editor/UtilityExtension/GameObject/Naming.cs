﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using EGL = UnityEditor.EditorGUILayout;

/// <summary>
/// Author : 정상철
/// </summary>
namespace Sions
{
    public class ObjectNamingWindow : EditorWindow
    {

        [MenuItem("GameObject/Naming/Format")]
        public static void Showing()
        {
            GetWindowWithRect<ObjectNamingWindow>(new Rect(0, 0, 300, 80), true, "Naming", true);
        }

        public void OnEnable()
        {
            format = EditorPrefs.GetString("ObjectNamingWindowFormat", "GameObject {0:D3}");
            indexStartOffset = EditorPrefs.GetInt("ObjectNamingWindowOffset", 0);
        }

        public string format;
        public int indexStartOffset;

        public void OnGUI()
        {
            EGL.Space();
            format = EGL.TextField("Format", format);
            indexStartOffset = EGL.IntField("Index Start Offset", indexStartOffset);
            if (GUI.changed)
            {
                EditorPrefs.SetString("ObjectNamingWindowFormat", format);
                EditorPrefs.SetInt("ObjectNamingWindowOffset", indexStartOffset);
            }
            EGL.Space();

            EGL.BeginHorizontal();
            EditorGUILayout.Space();
            if (GUILayout.Button("Change", GUILayout.Width(100)))
            {
                Transform[] trans = Selection.transforms;
                Array.Sort(trans, (a, b) =>
                {
                    return a.GetSiblingIndex().CompareTo(b.GetSiblingIndex());
                });

                for (int i = 0; i < trans.Length; i++)
                {
                    string format2 = format.Replace("{-}", trans[i].name);
                    trans[i].name = string.Format(format2, i + indexStartOffset);
                }
            }
            EGL.EndHorizontal();
        }

    }
}