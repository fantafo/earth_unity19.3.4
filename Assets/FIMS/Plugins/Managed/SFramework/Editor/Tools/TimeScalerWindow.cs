﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

namespace Sions.Editors
{
    public class TimeScalerWindow : EditorWindow
    {
        [MenuItem("SF/Time Scaler %&s", priority = 0x1000)]
        public static void ShowWindow()
        {
            EditorWindow.GetWindow<TimeScalerWindow>();
        }

        void OnGUI()
        {
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            {
                GUILayout.Space(10);

                GUILayout.BeginVertical();
                {

                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("TimeScale");
                        string text = GUILayout.TextField(Time.timeScale.ToString());
                        if (!string.IsNullOrEmpty(text))
                        {
                            float scale;
                            if (float.TryParse(text, out scale))
                            {
                                SetTimeScale(scale);
                                RemoveNotification();
                            }
                            else
                            {
                                ShowNotification(new GUIContent("숫자만 입력 해 주세요."));
                            }
                        }
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.Space(10);

                    GUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("<<"))
                        {
                            SetTimeScale(Time.timeScale - 1f);
                        }
                        if (GUILayout.Button("<"))
                        {
                            SetTimeScale(Time.timeScale - 0.1f);
                        }
                        if (GUILayout.Button(">"))
                        {
                            SetTimeScale(Time.timeScale + 0.1f);
                        }
                        if (GUILayout.Button(">>"))
                        {
                            SetTimeScale(Time.timeScale + 1f);
                        }
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.Space(10);

                    if (GUILayout.Button("x0.1"))
                    {
                        SetTimeScale(.1f);
                    }
                    if (GUILayout.Button("x0.5"))
                    {
                        SetTimeScale(.5f);
                    }
                    if (GUILayout.Button("x1"))
                    {
                        SetTimeScale(1);
                    }
                    if (GUILayout.Button("x3"))
                    {
                        SetTimeScale(3);
                    }
                    if (GUILayout.Button("x5"))
                    {
                        SetTimeScale(5);
                    }
                    if (GUILayout.Button("x10"))
                    {
                        SetTimeScale(10);
                    }
                    if (GUILayout.Button("x20"))
                    {
                        SetTimeScale(20);
                    }
                    if (GUILayout.Button("x50"))
                    {
                        SetTimeScale(50);
                    }
                    if (GUILayout.Button("x100"))
                    {
                        SetTimeScale(100);
                    }
                }
                GUILayout.EndVertical();

                GUILayout.Space(10);
            }
            GUILayout.EndHorizontal();
        }

        void SetTimeScale(float scale)
        {
            if (Application.isPlaying)
            {
                float updateScale = scale / Time.timeScale;
                Time.timeScale = scale;

                foreach (var p in GameObject.FindObjectsOfType<AudioSource>())
                {
                    p.pitch *= updateScale;
                }
            }
        }
    }
}