﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;

namespace SFramework
{
    public struct SArray<T>
        where T : class
    {
        public T[] array;

        public int Length
        {
            get
            {
                return array.Length;
            }
        }

        public bool IsEmpty
        {
            get
            {
                return array == null || array.Length == 0;
            }
        }

        public bool IsNullOrEmpty
        {
            get
            {
                if (array == null || array.Length == 0)
                    return true;
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] != null)
                        return false;
                }
                return true;
            }
        }

        public void Add(T item)
        {
            array = array.Add(item);
        }

        public void AddRange(T[] items)
        {
            array = array.AddRange(items);
        }

        public void Insert(int index, T item)
        {
            array = array.Insert(index, item);
        }

        public void InsertRange(int index, T[] items)
        {
            array = array.InsertRange(index, items);
        }

        public bool Remove(T item)
        {
            bool result;
            array = array.Remove(item, out result);
            return result;
        }

        public void RemoveAt(int index)
        {
            array = array.RemoveAt(index);
        }

        public void RemoveRange(int index, int count)
        {
            array = array.RemoveRange(index, count);
        }

        public void Reverse()
        {
            System.Array.Reverse(array);
        }

        public void Set(T item)
        {
            array.Set(item);
        }

        public int IndexOf(T value)
        {
            return System.Array.IndexOf(array, value);
        }

        public int IndexOf(T value, int startIndex)
        {
            return System.Array.IndexOf(array, value, startIndex);
        }

        public int IndexOf(T value, int startIndex, int count)
        {
            return System.Array.IndexOf(array, value, startIndex, count);
        }

        public int LastIndexOf(T value)
        {
            return System.Array.LastIndexOf(array, value);
        }

        public int LastIndexOf(T value, int startIndex)
        {
            return System.Array.LastIndexOf(array, value);
        }

        public int LastIndexOf(T value, int startIndex, int count)
        {
            return System.Array.LastIndexOf(array, value);
        }

        public static implicit operator SArray<T>(T[] array)
        {
            return new SArray<T> { array = array };
        }

        public static implicit operator T[] (SArray<T> src)
        {
            return src.array;
        }
    }
}