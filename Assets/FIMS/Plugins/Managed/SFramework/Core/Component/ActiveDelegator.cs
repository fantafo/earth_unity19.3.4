﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class ActiveDelegator : SMonoBehaviour
{
    public enum CompareType
    {
        And, Or
    }

    public Object[] delegator;
    public Object[] delegatee;
    public CompareType compare;

    private void OnEnable()
    {
        if(delegator == null
            || delegator.Length == 0
            || delegatee == null
            || delegatee.Length == 0)
        {
            enabled = false;
            return;
        }
    }
    private void OnValidate()
    {
        OnEnable();
    }

    private void Update()
    {
        bool active = false;
        foreach(var obj in delegator)
        {
            if(compare == CompareType.And)
            {
                if(obj.IsComplexActiveSelf())
                {
                    active = true;
                }
                else
                {
                    active = false;
                    break;
                }
            }
            else if(compare == CompareType.Or)
            {
                if (obj.IsComplexActiveSelf())
                {
                    active = true;
                    break;
                }
            }
        }

        foreach (var obj in delegatee)
            obj.SetComplexActiveSelf(active);
    }

}