﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class DontDestroyOnLoad : SMonoBehaviour
{
    private void Awake()
    {
        transform.SetParent(null, true);
        DontDestroyOnLoad(gameObject);
    }
}