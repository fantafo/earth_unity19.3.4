﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/Tracker/Transform Tracker")]
[ExecuteInEditMode]
public class TransformTracker : AbsTracker
{
    [Space]
    public Transform _target;
    public Vector3 _offsetPos;
    public bool _trackPosX = true;
    public bool _trackPosY = true;
    public bool _trackPosZ = true;

    public Vector3 _offsetRot;
    public bool _trackRotX = true;
    public bool _trackRotY = true;
    public bool _trackRotZ = true;


    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null)
            return;
#endif
        Vector3 rot = (_target.rotation * Quaternion.Euler(_offsetRot)).eulerAngles;
        if (!_trackRotX)
            rot.x = 0;
        if (!_trackRotY)
            rot.y = 0;
        if (!_trackRotZ)
            rot.z = 0;

        var mPos = transform.position;
        var tPos = _target.position + Quaternion.Euler(rot) * _offsetPos;

        if (!_trackPosX)
            tPos.x = mPos.x;
        if (!_trackPosY)
            tPos.y = mPos.y;
        if (!_trackPosZ)
            tPos.z = mPos.z;

        transform.position = tPos;
    }
}