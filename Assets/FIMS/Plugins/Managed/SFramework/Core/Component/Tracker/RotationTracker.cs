﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/Tracker/Rotation Tracker")]
[ExecuteInEditMode]
public class RotationTracker : AbsTracker
{
    [Space]
    public Transform _target;
    public Vector3 _offset;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null)
            return;
#endif
        transform.rotation = _target.rotation * Quaternion.Euler(_offset);
    }
}