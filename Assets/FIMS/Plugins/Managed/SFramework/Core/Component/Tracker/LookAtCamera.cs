﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

[ExecuteInEditMode]
public class LookAtCamera : AbsTracker
{
    public Camera _target;
    public Vector3 _offset;
    public bool onlyY;

    void Reset()
    {
        if (transform is RectTransform)
            _offset = new Vector3(0, 180, 0);
    }

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null && Camera.main == null)
            return;
#endif
        Transform target = (_target != null ? _target.transform : Camera.main.transform);

        if (onlyY)
        {
            transform.rotation = Quaternion.LookRotation((target.position - transform.position).ResetY()) * Quaternion.Euler(_offset);
        }
        else
        { 
            transform.rotation = Quaternion.LookRotation(target.position - transform.position) * Quaternion.Euler(_offset);
        }
    }
}