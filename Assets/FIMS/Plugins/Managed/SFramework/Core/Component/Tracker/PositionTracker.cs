﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/Tracker/Position Tracker")]
[ExecuteInEditMode]
public class PositionTracker : AbsTracker
{
    [Space]
    public Transform _target;
    public Vector3 _offset;
    public bool _trackX = true;
    public bool _trackY = true;
    public bool _trackZ = true;

    public override void OnReposition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && _target == null)
            return;
#endif
        var mPos = transform.position;
        var tPos = _target.position + _offset;

        if (!_trackX)
            tPos.x = mPos.x;
        if (!_trackY)
            tPos.y = mPos.y;
        if (!_trackZ)
            tPos.z = mPos.z;

        transform.position = tPos;
    }
}