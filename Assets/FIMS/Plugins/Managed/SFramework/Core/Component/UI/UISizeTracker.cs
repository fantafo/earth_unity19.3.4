﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[AddComponentMenu("SF/UI/Size Tracker")]
[RequireComponent(typeof(RectTransform))]
public class UISizeTracker : AbsTracker
{
    [Space]
    public RectTransform _target;
    public Vector2 padding;

    Vector2 befSize;

    public override void OnReposition()
    {
        Vector2 size = _target.rect.size + padding;
        if (size != befSize)
        {
            befSize = size;
            ((RectTransform)transform).SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
            ((RectTransform)transform).SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
        }
    }
}