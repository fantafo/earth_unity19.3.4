﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;

public static class DelegateExtension
{
    public static void Run(this Action act)
    {
        if (act != null)
            act();
    }
    public static void Run<A>(this Action<A> act, A val)
    {
        if (act != null)
            act(val);
    }
    public static void Run<A, B>(this Action<A, B> act, A val, B val2)
    {
        if (act != null)
            act(val, val2);
    }
    public static void Run<A, B, C>(this Action<A, B, C> act, A val, B val2, C val3)
    {
        if (act != null)
            act(val, val2, val3);
    }

    public static R Run<R>(this Func<R> act)
    {
        if (act != null)
            return act();
        return default(R);
    }
    public static R Run<A, R>(this Func<A, R> act, A val)
    {
        if (act != null)
            return act(val);
        return default(R);
    }
    public static R Run<A, B, R>(this Func<A, B, R> act, A val, B val2)
    {
        if (act != null)
            return act(val, val2);
        return default(R);
    }
    public static R Run<A, B, C, R>(this Func<A, B, C, R> act, A val, B val2, C val3)
    {
        if (act != null)
            return act(val, val2, val3);
        return default(R);
    }

    public static R Run<R>(this Func<R> act, R def)
    {
        if (act != null)
            return act();
        return def;
    }
    public static R Run<A, R>(this Func<A, R> act, A val, R def)
    {
        if (act != null)
            return act(val);
        return def;
    }
    public static R Run<A, B, R>(this Func<A, B, R> act, A val, B val2, R def)
    {
        if (act != null)
            return act(val, val2);
        return def;
    }
    public static R Run<A, B, C, R>(this Func<A, B, C, R> act, A val, B val2, C val3, R def)
    {
        if (act != null)
            return act(val, val2, val3);
        return def;
    }
}