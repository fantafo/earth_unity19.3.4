﻿using System;
using System.IO;
using UnityEngine;

public static class BinaryStreamExtension
{
    //================================================================================================
    //
    //                                        Writeable
    //
    //================================================================================================
    public static void WriteRGB(this BinaryWriter self, Color32 value)
    {
        self.Write(value.r);
        self.Write(value.g);
        self.Write(value.b);
    }
    public static void WriteRGBA(this BinaryWriter self, Color32 value)
    {
        self.Write(value.r);
        self.Write(value.g);
        self.Write(value.b);
        self.Write(value.a);
    }
    public static void WriteV3(this BinaryWriter self, Vector3 value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.z);
    }
    /// <summary>
    /// Normal 벡터를 3바이트로 보냅니다. -1 ~ 1 까지 사용할 수 있습니다.
    /// </summary>
    public static void WriteNormal(this BinaryWriter self, Vector3 value)
    {
        self.Write((sbyte)(Mathf.Clamp(value.x, -1, 1) * sbyte.MaxValue));
        self.Write((sbyte)(Mathf.Clamp(value.y, -1, 1) * sbyte.MaxValue));
        self.Write((sbyte)(Mathf.Clamp(value.z, -1, 1) * sbyte.MaxValue));
    }
    /// <summary>
    /// Normal 벡터를 4바이트로 보냅니다. -1 ~ 1 까지 사용할 수 있습니다.
    /// </summary>
    public static void WriteNormal32(this BinaryWriter self, Vector3 value)
    {
        const int MAX_XZ = 1023; // (2^10-1) 11비트를 사용한 signed 최대 값 : 1023
        const int MAX_Y = 511;  // (2^9-1) 10비트를 사용한 signed 최대 값 : 511

        int memory = 0;

        memory |= (value.x < 0 ? 1 : 0) << 0;
        memory |= (value.y < 0 ? 1 : 0) << 1;
        memory |= (value.z < 0 ? 1 : 0) << 2;
        memory |= (int)(Mathf.Clamp(Mathf.Abs(value.x), -1, 1) * MAX_XZ) << 3;
        memory |= (int)(Mathf.Clamp(Mathf.Abs(value.y), -1, 1) * MAX_Y) << 13;
        memory |= (int)(Mathf.Clamp(Mathf.Abs(value.z), -1, 1) * MAX_XZ) << 22;

        self.Write(memory);
    }
    /// <summary>
    /// Normal 벡터를 6바이트로 보냅니다. -1 ~ 1 까지 사용할 수 있습니다. Normal보다 정밀합니다.
    /// </summary>
    public static void WriteNormal48(this BinaryWriter self, Vector3 value)
    {
        self.Write((short)(Mathf.Clamp(value.x, -1, 1) * Int16.MaxValue));
        self.Write((short)(Mathf.Clamp(value.y, -1, 1) * Int16.MaxValue));
        self.Write((short)(Mathf.Clamp(value.z, -1, 1) * Int16.MaxValue));
    }
    public static void WriteV2(this BinaryWriter self, Vector2 value)
    {
        self.Write(value.x);
        self.Write(value.y);
    }
    public static void WriteQ(this BinaryWriter self, Quaternion value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.z);
        self.Write(value.w);
    }
    public static void WriteEuler24(this BinaryWriter self, Vector3 value)
    {
        const float CALC_VALUE = (1f / 360f * 255);

        self.WriteC((byte)(((value.x + 3600) % 360) * CALC_VALUE));
        self.WriteC((byte)(((value.y + 3600) % 360) * CALC_VALUE));
        self.WriteC((byte)(((value.z + 3600) % 360) * CALC_VALUE));
    }
    public static void WriteEuler32(this BinaryWriter self, Vector3 value)
    {
        const float SCALE_XZ = 2048 / 360f; // 11비트를 360도로 나눈 값.
        const float SCALE_Y = 1024 / 360f;  // 10비트를 360도로 나눈 값

        int memory = 0;
        memory |= (int)(((value.x + 3600) % 360) * SCALE_XZ);
        memory |= (int)(((value.y + 3600) % 360) * SCALE_Y) << 11;
        memory |= (int)(((value.z + 3600) % 360) * SCALE_XZ) << 21;

        self.Write(memory);
    }
    public static void WriteEuler(this BinaryWriter self, Vector3 value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.z);
    }
    public static void WriteRect(this BinaryWriter self, Rect value)
    {
        self.Write(value.x);
        self.Write(value.y);
        self.Write(value.width);
        self.Write(value.height);
    }
    public static void WriteBounds(this BinaryWriter self, Bounds value)
    {
        self.WriteV3(value.center);
        self.WriteV3(value.extents);
    }

    public static void WriteB(this BinaryWriter self, bool value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, byte value) { self.Write(value); }
    public static void WriteC(this BinaryWriter self, int value) { self.Write((byte)value); }
    public static void WriteH(this BinaryWriter self, short value) { self.Write(value); }
    public static void WriteH(this BinaryWriter self, int value) { self.Write((short)value); }
    public static void WriteD(this BinaryWriter self, int value) { self.Write(value); }
    public static void WriteL(this BinaryWriter self, long value) { self.Write(value); }
    public static void WriteF(this BinaryWriter self, float value) { self.Write(value); }
    public static void WriteS(this BinaryWriter self, string value) { self.Write(value ?? string.Empty); }
    public static void WriteEC<T>(this BinaryWriter self, T v) where T : struct, IConvertible { self.WriteC(v.ToByte(null)); }
    public static void WriteEH<T>(this BinaryWriter self, T v) where T : struct, IConvertible { self.WriteH(v.ToInt16(null)); }


    //================================================================================================
    //
    //                                        Readable
    //
    //================================================================================================

    public static Color32 ReadColorRGB(this BinaryReader self)
    {
        return new Color32(self.ReadByte(), self.ReadByte(), self.ReadByte(), (byte)0xFF);
    }
    public static Color32 ReadColorRGBA(this BinaryReader self)
    {
        return new Color32(self.ReadByte(), self.ReadByte(), self.ReadByte(), self.ReadByte());
    }
    public static Vector2 ReadVector2(this BinaryReader self)
    {
        return new Vector2(self.ReadSingle(), self.ReadSingle());
    }
    public static Vector3 ReadVector3(this BinaryReader self)
    {
        return new Vector3(self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Vector3 ReadNormal(this BinaryReader self)
    {
        const float SCALE = 1f / sbyte.MaxValue;
        return new Vector3(self.ReadSByte() * SCALE, self.ReadSByte() * SCALE, self.ReadSByte() * SCALE);
    }
    public static Vector3 ReadNormal32(this BinaryReader self)
    {
        const int MAX_XZ = 1023; // (2^10-1) 11비트를 사용한 signed 최대 값 : 1023
        const int MAX_Y = 511;  // (2^9-1) 10비트를 사용한 signed 최대 값 : 511
        const float SCALE_XZ = 1f / MAX_XZ;
        const float SCALE_Y = 1f / MAX_Y;

        int memory = self.ReadInt32();
        var result = Vector3.zero;

        result.x = ((memory >> 3) & MAX_XZ) * ((memory & 1) == 1 ? -1 : 1) * SCALE_XZ;
        result.y = ((memory >> 13) & MAX_Y) * ((memory & 2) == 2 ? -1 : 1) * SCALE_Y;
        result.z = ((memory >> 22) & MAX_XZ) * ((memory & 4) == 4 ? -1 : 1) * SCALE_XZ;
        return result;
    }
    public static Vector3 ReadNormal48(this BinaryReader self)
    {
        const float SCALE = 1f / Int16.MaxValue;
        return new Vector3(self.ReadInt16() * SCALE, self.ReadInt16() * SCALE, self.ReadInt16() * SCALE);
    }
    public static Vector3 ReadEuler24(this BinaryReader self)
    {
        const float CALC_VALUE = (1f / 255f * 360f);
        return new Vector3(self.ReadC() * CALC_VALUE, self.ReadC() * CALC_VALUE, self.ReadC() * CALC_VALUE);
    }
    public static Vector3 ReadEuler32(this BinaryReader self)
    {
        const float SCALE_XZ = 1f / (2048f / 360f); // 11비트를 360도로 나눈 값.
        const float SCALE_Y = 1f / (1024f / 360f);  // 10비트를 360도로 나눈 값
        const int MAX_XZ = 2047;
        const int MAX_Y = 1023;

        int memory = self.ReadInt32();
        var result = Vector3.zero;

        result.x = ((memory) & MAX_XZ) * SCALE_XZ;
        result.y = ((memory >> 11) & MAX_Y) * SCALE_Y;
        result.z = ((memory >> 22) & MAX_XZ) * SCALE_XZ;
        return result;
    }
    public static Vector3 ReadEuler(this BinaryReader self)
    {
        return new Vector3(self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Quaternion ReadQuaternion(this BinaryReader self)
    {
        return new Quaternion(self.ReadSingle(), self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Rect ReadRect(this BinaryReader self)
    {
        return new Rect(self.ReadSingle(), self.ReadSingle(), self.ReadSingle(), self.ReadSingle());
    }
    public static Bounds ReadBounds(this BinaryReader self)
    {
        return new Bounds(self.ReadVector3(), self.ReadVector3());
    }

    public static bool ReadB(this BinaryReader self) { return self.ReadBoolean(); }
    public static byte ReadC(this BinaryReader self) { return self.ReadByte(); }
    public static short ReadH(this BinaryReader self) { return self.ReadInt16(); }
    public static int ReadD(this BinaryReader self) { return self.ReadInt32(); }
    public static long ReadL(this BinaryReader self) { return self.ReadInt64(); }
    public static float ReadF(this BinaryReader self) { return self.ReadSingle(); }
    public static string ReadS(this BinaryReader self) { return self.ReadString(); }
    public static T ReadEC<T>(this BinaryReader self) where T : struct, IConvertible { return (T)Enum.ToObject(typeof(T), self.ReadByte()); }
    public static T ReadEH<T>(this BinaryReader self) where T : struct, IConvertible { return (T)Enum.ToObject(typeof(T), self.ReadInt16()); }

}