﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

public static class ColorExtension
{
    public static Color SetR(this Color _this, float r)
    {
        _this.r = r;
        return _this;
    }
    public static Color SetG(this Color _this, float g)
    {
        _this.g = g;
        return _this;
    }
    public static Color SetB(this Color _this, float b)
    {
        _this.b = b;
        return _this;
    }
    public static Color SetAlpha(this Color _this, float a)
    {
        _this.a = a;
        return _this;
    }

    public static Color Normalize(this Color _this)
    {
        _this.a = Mathf.Clamp01(_this.a);
        _this.r = Mathf.Clamp01(_this.r);
        _this.g = Mathf.Clamp01(_this.g);
        _this.b = Mathf.Clamp01(_this.b);
        return _this;
    }
}