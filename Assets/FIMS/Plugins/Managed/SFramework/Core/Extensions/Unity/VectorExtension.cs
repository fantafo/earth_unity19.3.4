﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

public static class VectorExtensions
{
    #region Vector3
    public static Vector3 ZEROe3(this Vector3 a)
    {
        if (Mathf.Abs(a.x) < 0.001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.001)
            a.y = 0;
        if (Mathf.Abs(a.z) < 0.001)
            a.z = 0;
        return a;
    }
    public static Vector3 ZEROe4(this Vector3 a)
    {
        if (Mathf.Abs(a.x) < 0.0001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.0001)
            a.y = 0;
        if (Mathf.Abs(a.z) < 0.0001)
            a.z = 0;
        return a;
    }
    public static Vector3 ZEROe5(this Vector3 a)
    {
        if (Mathf.Abs(a.x) < 0.00001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.00001)
            a.y = 0;
        if (Mathf.Abs(a.z) < 0.00001)
            a.z = 0;
        return a;
    }
    public static Vector3 ResetY(this Vector3 a) { a.y = 0; return a; }
    public static Vector3 ResetX(this Vector3 a) { a.x = 0; return a; }
    public static Vector3 ResetZ(this Vector3 a) { a.z = 0; return a; }
    public static Vector3 OnlyX(this Vector3 a) { return new Vector3(a.x, 0, 0); }
    public static Vector3 OnlyY(this Vector3 a) { return new Vector3(0, a.y, 0); }
    public static Vector3 OnlyZ(this Vector3 a) { return new Vector3(0, 0, a.z); }
    public static Vector3 SetX(this Vector3 a, float x) { a.x = x; return a; }
    public static Vector3 SetY(this Vector3 a, float y) { a.y = y; return a; }
    public static Vector3 SetZ(this Vector3 a, float z) { a.z = z; return a; }

    public static Vector3 SetXY(this Vector3 a, float xy) { a.x = a.y = xy; return a; }
    public static Vector3 SetYZ(this Vector3 a, float yz) { a.y = a.z = yz; return a; }
    public static Vector3 SetXZ(this Vector3 a, float xz) { a.x = a.z = xz; return a; }

    public static Vector3 MultiX(this Vector3 a, float x) { a.x *= x; return a; }
    public static Vector3 MultiY(this Vector3 a, float y) { a.y *= y; return a; }
    public static Vector3 MultiZ(this Vector3 a, float z) { a.z *= z; return a; }
    public static Vector3 MultiXY(this Vector3 a, float xy) { a.x *= xy; a.x *= xy; return a; }
    public static Vector3 MultiYZ(this Vector3 a, float yz) { a.y *= yz; a.z *= yz; return a; }
    public static Vector3 MultiXZ(this Vector3 a, float xz) { a.x *= xz; a.z *= xz; return a; }

    public static Vector3 Multiply(this Vector3 a, Vector3 b)
    {
        a.x *= b.x;
        a.y *= b.y;
        a.z *= b.z;
        return a;
    }
    public static Vector3 Divide(this Vector3 a, Vector3 b)
    {
        a.x /= b.x;
        a.y /= b.y;
        a.z /= b.z;
        return a;
    }

    public static Quaternion ToLookQuaternion(this Vector3 direction)
    {
        return Quaternion.LookRotation(direction);
    }

    public static Quaternion EulerToQuaternion(this Vector3 euler)
    {
        return Quaternion.Euler(euler);
    }

    public static Vector3[] Plus(this Vector3[] src, Vector3 v)
    {
        Vector3[] result = new Vector3[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i] + v;
        }
        return result;
    }
    public static Vector3[] Multiply(this Vector3[] src, Vector3 v)
    {
        Vector3[] result = new Vector3[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i].Multiply(v);
        }
        return result;
    }
    public static Vector3[] Divide(this Vector3[] src, Vector3 v)
    {
        Vector3[] result = new Vector3[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i].Divide(v);
        }
        return result;
    }

    public static float MaxValue(this Vector3 src)
    {
        return Mathf.Max(src.x, src.y, src.z);
    }
    public static float MinValue(this Vector3 src)
    {
        return Mathf.Min(src.x, src.y, src.z);
    }
    public static float Average(this Vector3 src)
    {
        return (src.x + src.y + src.z) * 0.333333333333333333333333333333333333333333333333f;
    }

    public static Vector3 NearPoint(this Vector3[] src, Vector3 point)
    {
        Vector3 result = point;
        float minDistance = float.MaxValue, dist;
        for (int i = 0; i < src.Length; i++)
        {
            dist = Vector3.Distance(src[i], point);
            if (dist < minDistance)
            {
                result = src[i];
                minDistance = dist;
            }
        }
        return result;
    }

    public static bool IsNaN(this Vector3 pos)
    {
        return float.IsNaN(pos.x) || float.IsNaN(pos.y) || float.IsNaN(pos.z);
    }
    #endregion

    #region Vector2
    public static Vector2 ZEROe3(this Vector2 a)
    {
        if (Mathf.Abs(a.x) < 0.001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.001)
            a.y = 0;
        return a;
    }
    public static Vector2 ZEROe4(this Vector2 a)
    {
        if (Mathf.Abs(a.x) < 0.0001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.0001)
            a.y = 0;
        return a;
    }
    public static Vector2 ZEROe5(this Vector2 a)
    {
        if (Mathf.Abs(a.x) < 0.00001)
            a.x = 0;
        if (Mathf.Abs(a.y) < 0.00001)
            a.y = 0;
        return a;
    }
    public static Vector2 ResetY(this Vector2 a) { a.y = 0; return a; }
    public static Vector2 ResetX(this Vector2 a) { a.x = 0; return a; }
    public static Vector2 OnlyX(this Vector2 a) { return new Vector2(a.x, 0); }
    public static Vector2 OnlyY(this Vector2 a) { return new Vector2(0, a.y); }
    public static Vector2 SetX(this Vector2 a, float x) { a.x = x; return a; }
    public static Vector2 SetY(this Vector2 a, float y) { a.y = y; return a; }

    public static Vector2 SetXY(this Vector2 a, float xy) { a.x = a.y = xy; return a; }

    public static Vector2 MultiX(this Vector2 a, float x) { a.x *= x; return a; }
    public static Vector2 MultiY(this Vector2 a, float y) { a.y *= y; return a; }
    public static Vector2 MultiXY(this Vector2 a, float xy) { a.x *= xy; a.x *= xy; return a; }

    public static Vector2 Multiply(this Vector2 a, Vector2 b)
    {
        a.x *= b.x;
        a.y *= b.y;
        return a;
    }
    public static Vector2 Divide(this Vector2 a, Vector2 b)
    {
        a.x /= b.x;
        a.y /= b.y;
        return a;
    }

    public static Vector2[] Plus(this Vector2[] src, Vector2 v)
    {
        Vector2[] result = new Vector2[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i] + v;
        }
        return result;
    }
    public static Vector2[] Multiply(this Vector2[] src, Vector2 v)
    {
        Vector2[] result = new Vector2[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i].Multiply(v);
        }
        return result;
    }
    public static Vector2[] Divide(this Vector2[] src, Vector2 v)
    {
        Vector2[] result = new Vector2[src.Length];
        for (int i = 0; i < src.Length; i++)
        {
            result[i] = src[i].Divide(v);
        }
        return result;
    }

    public static float MaxValue(this Vector2 src)
    {
        return Mathf.Max(src.x, src.y);
    }
    public static float MinValue(this Vector2 src)
    {
        return Mathf.Min(src.x, src.y);
    }
    public static float Average(this Vector2 src)
    {
        return (src.x + src.y) * 0.5f;
    }
    #endregion

}