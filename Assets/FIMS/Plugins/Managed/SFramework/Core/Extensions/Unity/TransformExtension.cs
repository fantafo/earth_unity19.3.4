﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using UnityEngine;

public static class TransformExtension
{
    public static void DestroyChildren(this Transform transform)
    {
        for (int i = 0, len = transform.childCount; i < len; i++)
        {
            GameObject.Destroy(transform.GetChild(i).gameObject);
        }
    }
    public static void DestroyChildrenImmediate(this Transform transform)
    {
        while (transform.childCount > 0)
        {
            GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    public static Transform FindChildDeep(this Transform trans, string name)
    {
        Transform tf = trans.Find(name);
        if (tf != null)
            return tf;

        for (int i = 0, len = trans.childCount; i < len; i++)
        {
            tf = trans.GetChild(i).FindChildDeep(name);
            if (tf != null)
                return tf;
        }
        return null;
    }

    public static Transform FindChildTag(this Transform trans, string tag)
    {
        Transform tf;
        for (int i = 0, len = trans.childCount; i < len; i++)
        {
            tf = trans.GetChild(i);
            if (tf.CompareTag(tag))
            {
                return tf;
            }
            else
            {
                tf = tf.FindChildTag(tag);
                if (tf != null)
                    return tf;
            }
        }
        return null;
    }

    public static void ResetWorld(this Transform trans)
    {
        trans.position = Vector3.zero;
        trans.rotation = Quaternion.identity;
        trans.localScale = Vector3.one;
    }

    public static void ResetLocal(this Transform trans)
    {
        trans.localPosition = Vector3.zero;
        trans.localRotation = Quaternion.identity;
        trans.localScale = Vector3.one;
    }

    public static void CopyFrom(this Transform dest, Transform src)
    {
        dest.parent = src.parent;
        dest.SetSiblingIndex(src.GetSiblingIndex());
        dest.localPosition = src.localPosition;
        dest.localRotation = src.localRotation;
        dest.localScale = src.localScale;
    }

    public static bool IsParent(this Transform child, Transform parent)
    {
        while ((child = child.parent) != parent && child != null)
            ;
        return child == parent;
    }

    public static string Path(this Transform trans)
    {
        string path = trans.name;
        while ((trans = trans.parent) != null)
        {
            path = trans.name + "/" + path;
        }
        return path;
    }
}