﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using SFramework;
using System.Collections.Generic;
using UnityEngine;

public static class ComponentExtempsion
{
    public static void ToggleEnable(this Behaviour comp)
    {
        comp.enabled = !comp.enabled;
    }
    public static void ToggleActive(this Component comp)
    {
        comp.gameObject.ToggleActive();
    }
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(go.activeSelf);
    }
    public static WaitForSeconds Wait(this MonoBehaviour go, float duration)
    {
        return Waits.Wait(duration);
    }

    public static void SetEnableChildren<T>(this Component comp, bool enable)
        where T : Behaviour
    {
        List<T> list = ListPool<T>.Take();
        comp.GetComponentsInChildren<T>(enable, list);
        for (int i = 0; i < list.Count; i++)
            list[i].enabled = false;
        list.Release();
    }
    public static void SetEnableChildrenRenderer<T>(this Component comp, bool enable)
        where T : Renderer
    {
        List<T> list = ListPool<T>.Take();
        comp.GetComponentsInChildren<T>(enable, list);
        for (int i = 0; i < list.Count; i++)
            list[i].enabled = enable;
        list.Release();
    }
    public static void DestroyChildren<T>(this Component comp, bool includeInactive)
    {
        List<T> list = ListPool<T>.Take();
        comp.GetComponentsInChildren<T>(includeInactive, list);
        for (int i = 0; i < list.Count; i++)
        { GameObject.Destroy((Object)(object)list[i]); }
        list.Release();
    }
}