﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UObject = UnityEngine.Object;
using URandom = UnityEngine.Random;
using IFDEF = System.Diagnostics.ConditionalAttribute;
using System.Text;

public class SMonoBehaviour : MonoBehaviour
{
#if UNITY_EDITOR
    #region Log
    [IFDEF("UNITY_EDITOR")]
    protected void Log(object message)
    {
        Debug.Log(message);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void Logs(params object[] message)
    {
        StringBuilder sb = new StringBuilder();
        foreach (object o in message)
        {
            sb.AppendTab(o);
        }
        Debug.Log(sb.ToString());
    }
    [IFDEF("UNITY_EDITOR")]
    protected void Log(object message, UObject context)
    {
        Debug.Log(message, context);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogAssertion(object message)
    {
        Debug.LogAssertion(message);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogAssertion(object message, UObject context)
    {
        Debug.LogAssertion(message, context);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogAssertionFormat(string format, params object[] args)
    {
        Debug.LogAssertionFormat(format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogAssertionFormat(UObject context, string format, params object[] args)
    {
        Debug.LogAssertionFormat(context, format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogError(object message)
    {
        Debug.LogError(message);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogError(object message, UObject context)
    {
        Debug.LogError(message, context);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogErrorFormat(string format, params object[] args)
    {
        Debug.LogErrorFormat(format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogErrorFormat(UObject context, string format, params object[] args)
    {
        Debug.LogErrorFormat(context, format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogException(System.Exception exception)
    {
        Debug.LogException(exception);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogException(System.Exception exception, UObject context)
    {
        Debug.LogException(exception, context);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogFormat(string format, params object[] args)
    {
        Debug.LogFormat(format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogFormat(UObject context, string format, params object[] args)
    {
        Debug.LogFormat(context, format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogWarning(object message)
    {
        Debug.LogWarning(message);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogWarning(object message, UObject context)
    {
        Debug.LogWarning(message, context);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogWarningFormat(string format, params object[] args)
    {
        Debug.LogWarningFormat(format, args);
    }
    [IFDEF("UNITY_EDITOR")]
    protected void LogWarningFormat(UObject context, string format, params object[] args)
    {
        Debug.LogWarningFormat(context, format, args);
    }
    #endregion

    #region Dialog
    [IFDEF("UNITY_EDITOR")]
    protected void ShowDialog(string message)
    {
        UnityEditor.EditorUtility.DisplayDialog(name + "(" + GetType().Name + ")", message, "OK");
    }
    protected bool ShowConfirm(string message, string ok = "OK", string cancel = "Cancel")
    {
        return UnityEditor.EditorUtility.DisplayDialog(name + "(" + GetType().Name + ")", message, ok, cancel);
    }
    #endregion
#endif

}