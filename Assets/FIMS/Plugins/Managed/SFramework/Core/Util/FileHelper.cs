﻿using System.Collections.Generic;
using System.Text;
using System.IO;

public static class FileHelper
{
    public static FileStream OpenRead(string path)
    {
        return File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
    }
    public static FileStream OpenWrite(string path)
    {
        return File.Open(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite);
    }
    public static byte[] ReadAllBytes(string path)
    {
        byte[] data;
        using (var fs = File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            data = new byte[fs.Length];
            fs.Read(data, 0, data.Length);
        }
        return data;
    }
    public static string ReadAllText(string path, Encoding enc = null)
    {
        if (enc == null)
            enc = Encoding.Default;

        byte[] data = ReadAllBytes(path);

        byte[] bom = enc.GetPreamble();
        bool useBOM = true;
        for (int i = 0; i < bom.Length; i++)
        {
            if (bom[i] != data[i])
            {
                useBOM = false;
                break;
            }
        }

        if (useBOM)
        {
            return enc.GetString(data, bom.Length, data.Length - bom.Length);
        }
        else
        {
            return enc.GetString(data);
        }

    }
    public static string[] ReadAllLines(string path, Encoding enc = null)
    {
        if (enc == null)
            enc = Encoding.Default;

        var bytes = ReadAllBytes(path);

        int count = 0;
        for (int i = 0; i < bytes.Length; i++)
        {
            if (bytes[i] == '\n')
            {
                count++;
            }
        }

        string[] data = new string[count];
        int inputIndex = 0;
        int offset = 0;
        int j = 0;
        for (; j < bytes.Length; j++)
        {
            switch (bytes[j])
            {
                case (byte)'\r':
                case (byte)'\n':
                    if (j != offset)
                    {
                        data[inputIndex++] = enc.GetString(bytes, offset, j - offset);
                        offset = j;
                    }
                    offset++;
                    break;
            }
        }

        return data;
    }
}