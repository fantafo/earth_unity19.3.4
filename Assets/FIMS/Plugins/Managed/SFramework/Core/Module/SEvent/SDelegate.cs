﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#if ENABLE_SEVENT
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UObject = UnityEngine.Object;

public class SDelegate : ScriptableObject
{
    public const BindingFlags METHOD_FLAG = BindingFlags.Instance | BindingFlags.Public;

    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Variables                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    // cache
    bool _isInit;
    MethodInfo _cacheMethod;
    ParameterInfo[] _cacheParams;
    object[] _cacheObjs;
    Action _cacheCallback;

    // serialize
    [SerializeField] UObject _target;
    [SerializeField] string _method;
    [SerializeField] string _validateName;
    [SerializeField] SDelegateParameter[] _params;




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Properties                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public UObject target
    {
        get { return _target; }
        set
        {
            if (_target != value)
            {
                if (_target == null || _target.GetType() != value.GetType()) Reload();
                _target = value;
            }
        }
    }

    public string methodName
    {
        get { return _method; }
        set
        {
            if (_method != value)
            {
                _method = value;
                Reload();
            }
        }
    }

    public MethodInfo method
    {
        get { return _cacheMethod; }
        set
        {
            if (_cacheMethod != value)
            {
                if (value != null)
                {
                    _method = value.Name;
                    _validateName = ConvertMethodName(value);
                    Reload();
                }
                else
                {
                    _method = null;
                    _validateName = null;
                    _params = null;

                    _cacheMethod = null;
                    _cacheObjs = null;
                    _cacheCallback = null;
                    Reload();
                }
            }
        }
    }

    public SDelegateParameter[] parameters
    {
        get { return _params; }
        set
        {
            _params = value;
        }
    }

    public bool IsReload { get { return IsReload; } }

    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                          Constructor                        //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    // TODO : Delegate 등 쉽게 사용할 수 있는 방안을 추가해야함.
    public SDelegate() { }
    public SDelegate(Action action) { SetCallback(action); }
    public SDelegate(UObject obj, string name) { SetCallback(obj, name); }


    public void SetCallback(Action action)
    {
        _isInit = true;
        _cacheCallback = action;

#if UNITY_EDITOR
        if (action.Target is UObject)
        {
            _target = (UObject) action.Target;
            if(_cacheMethod != action.Method)
            {
                _method = action.Method.Name;
                _validateName = ConvertMethodName(action.Method);
                _cacheMethod = action.Method;
                _cacheParams = _cacheMethod.GetParameters();
                _cacheObjs = new object[_cacheParams.Length];
            }
        }
#endif
    }

    public void SetCallback(UObject obj, string name)
    {
        Reload();
        _target = obj;
        _method = name;
        _validateName = null;
    }





    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Preparing                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public void Reload()
    {
        _isInit = false;
    }

    /// <summary>
    /// Prepared Method
    /// </summary>
    public void Prepare()
    {
        if (_isInit) return;
        if (string.IsNullOrEmpty(_method)) return;

        _cacheMethod = null;
        _cacheCallback = null;

        // Validate Caching
        if(_target != null && !string.IsNullOrEmpty(_method))
        {
            // Find MethodInfo
            foreach(MethodInfo m in _target.GetType().GetMethods())
            {
                if(m.Name == _method)
                {
                    if (_cacheMethod == null) _cacheMethod = m;

                    if (string.IsNullOrEmpty(_validateName))
                    {
                        int paramCount = _params != null ? _params.Length : 0;
                        var methodParams = m.GetParameters();

                        if (methodParams.Length != paramCount) continue;

                        _cacheMethod = m;

                        break;
                    }
                    else
                    {
                        if (ConvertMethodName(m) == _validateName)
                        {
                            _cacheMethod = m;
                            break;
                        }
                    }
                }
            }

            if(_cacheMethod == null)
            {
                Debug.LogError("Could not find method '" + _method + "' on " + _target.GetType(), _target);
                return;
            }

            // valid Parameter
            _cacheParams = _cacheMethod.GetParameters();

            // If Parameter Length is 0. Create Delegate
            if (_cacheParams.Length == 0 && _cacheMethod.ReturnType == typeof(void))
            {
                _cacheCallback = (Action)Delegate.CreateDelegate(typeof(Action), _target, _method);
                _cacheParams = null;
                _cacheObjs = null;
                _params = null;
                return;
            }
            // Parameter Creation
            else
            {
                _cacheCallback = null;
                if(_params == null || _cacheParams.Length != _params.Length) _params = new SDelegateParameter[_cacheParams.Length];
                if (_cacheObjs == null || _cacheObjs.Length != _cacheParams.Length) _cacheObjs = new object[_params.Length];

                for (int i=0; i<_cacheParams.Length; i++)
                {
                    if (_params[i] == null) _params[i] = new SDelegateParameter();
                    _params[i].paramType = _cacheParams[i].ParameterType;
                }
            }
        }
    }





    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                           Execution                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public bool Execute()
    {
        Prepare();

        // call cache delegate
        if (_cacheCallback != null)
        {
#if !UNITY_EDITOR
            c_callback();
#else
            if (Application.isPlaying)
            {
                _cacheCallback();
            }
            else if (_cacheCallback.Target != null)
            {
                // Check [ExecuteInEditMode]
                System.Type type = _cacheCallback.Target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs != null && objs.Length > 0) _cacheCallback();
            }
#endif
            return true;
        }

        // call methodInfo
        else if (_cacheMethod != null)
        {
#if UNITY_EDITOR
            // Check [ExecuteInEditMode]
            if (_target != null && !Application.isPlaying)
            {
                System.Type type = _target.GetType();
                object[] objs = type.GetCustomAttributes(typeof(ExecuteInEditMode), true);
                if (objs == null || objs.Length == 0) return true;
            }
#endif

            // call (not argument method)
            if (_params == null || _params.Length == 0)
            {
                _cacheMethod.Invoke(_target, null);
            }
            // call (argument method)
            else
            {
                // set arguments
                for (int i = 0; i < _params.Length; i++)
                    _cacheObjs[i] = _params[i].Value;

                // invoke
                try
                {
                    _cacheMethod.Invoke(_target, _cacheObjs);
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
            }

            // set changed value to ref, out
            for (int i=0; i<_params.Length; i++)
            {
                if(_cacheParams[i].IsIn || _cacheParams[i].IsOut)
                {
                    _params[i].Value = _cacheObjs[i];
                }
            }
            return true;
        }
        return false;
    }




    /////////////////////////////////////////////////////////////////
    //                                                             //
    //                            Override                         //
    //                                                             //
    /////////////////////////////////////////////////////////////////

    public override string ToString()
    {
        Prepare();
        if (_target == null || string.IsNullOrEmpty(_method)) return "None";
        if (_cacheMethod == null) return ConvertObjectName(_target) + "/" + _method + "(Missing)";
        return ConvertObjectName(_target) + "/" + ConvertMethodName(_cacheMethod);
    }

    public static string ConvertObjectName(UObject comp)
    {
        Type type = comp.GetType();

        string objName = null;
        if (comp is MonoBehaviour)
        {
            MethodInfo mm = type.GetMethod("ToString");
            //if (mm.DeclaringType.IsSubclassOf(typeof(MonoBehaviour)))
            if (mm.DeclaringType != typeof(UnityEngine.Object))
            {
                objName = comp.ToString();
            }
        }
        if (objName == null) objName = type.Name;

        return objName;
    }

    private static System.Text.StringBuilder tempSB = new System.Text.StringBuilder();
    public static string ConvertMethodName(MethodInfo m)
    {
        tempSB.Remove(0, tempSB.Length);
        if (typeof(MonoBehaviour) == m.DeclaringType
            || typeof(MonoBehaviour).IsSubclassOf(m.DeclaringType))
        {
            tempSB.Append("*Base/");
        }
        if (m.IsSpecialName)
        {
            tempSB.Append("*Prop/");
        }

        tempSB.Append(m.Name).Append('(');
        bool first = true;
        foreach (var p in m.GetParameters())
        {
            if (!first) tempSB.Append(',');
            else first = false;
            tempSB.Append(SDelegateParameter.ConvertTypeName(p.ParameterType));
        }
        tempSB.Append(')');
        return tempSB.ToString();
    }
}
#endif