﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using UnityEngine;
using System.Collections.Generic;
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
using System.IO;
#endif

namespace SFramework
{
    /// <summary>
    /// Create a Texture2D showing Easing.
    /// </summary>
    public class EasingImage
    {
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
        // Temp storage Folder
        const string tempPath = "Temp/EasingImage/";
        static EasingImage()
        {
            try
            { Directory.CreateDirectory(tempPath); }
            catch { }
        }
#endif
        static Dictionary<int, Texture2D> caches = new Dictionary<int, Texture2D>();

        static int limitPixel = 20;
        static int width = 100;
        static int height = 100;

        static Color backgroundColor = new Color(.5f, .5f, .5f);
        static Color lineColor = Color.cyan;
        static Color limitColor = new Color(.6f, .6f, .6f);

        /// <summary>
        /// It shows the Texture2D caching.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Texture2D Get(SEasingType type)
        {
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
            string path = tempPath + type.ToString() + ".png";
            if (File.Exists(path))
            {
                try
                {
                    Texture2D fTex = new Texture2D(2, 2);
                    fTex.LoadImage(File.ReadAllBytes(path));
                    return fTex;
                }
                catch { }
            }
#endif
            Texture2D tex = caches.Get((int)type);
            if (tex == null)
            {
                tex = Make(SEasing.GetFunc(type));
                caches.Add((int)type, tex);
#if UNITY_EDITOR && !UNITY_WEBPLAYER && !UNITY_WEBGL
                try
                {
                    File.WriteAllBytes(path, tex.EncodeToPNG());
                }
                catch { }
#endif
            }
            return tex;
        }

        public static Texture2D Make(Func<float, float> func)
        {
            Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false, false);

            tex.FillColor(backgroundColor);

            tex.DrawLine(0, limitPixel, tex.width, limitPixel, limitColor);
            tex.DrawLine(0, tex.height - limitPixel, tex.width, tex.height - limitPixel, limitColor);

            int bx = 0, by = limitPixel;
            for (int x = 1; x < width; x++)
            {
                int y = (int)(func(((float)x / (width - 1))) * (height - (limitPixel * 2 + 1)) + limitPixel);

                if (y < 0)
                    y = 0;
                else if (y > tex.height)
                    y = tex.height;
                tex.DrawLine(bx, by, x, y, lineColor);

                bx = x;
                by = y;
            }

            tex.Apply(false);

            return tex;
        }
    }
}