1) 구성
	- GoogleVR, GearVR, ConsolePro, LeanTween, SFramework 플러그인
	
	- Fims.CommonServer와 통신하기 위한 
	- 핌스 어플(클라이언트) 간 통신을 위한 Command
	- 핌스 어플(클라이언트) 간 음성통화
	- 핌스 어플(클라이언트) 간 데이터 통합을 위한 Dictionary 제공 (실험적)
	- 핌스 Flag 처리를 구별하고 동작하는 컴포넌트 제공
	
	- 화면 Fade In,Out
	- VR장비 정보 통합, VRInput.cs (구글,기어 용  통합)
	- 해드트래킹 엑션, 컨트롤러 엑션 통합용 컴포넌트 제공
	
	- 메인 플레이어 기본타입 제공
	- 플레이어 기본 타입 및 모델 제공
	
	- 원클릭 빌드 시스템 (GearVR, Daydream, Cardboard) (유니티 매뉴 File/Build FTF)
	
2) SDK 구성
	FIMS/Editor		빌드 시스템
	FIMS/Import		플러그인들
	FIMS/Plugins	플러그인들
	FIMS/Models		캐릭터, 컨트롤러, 기본맵 등의 모델
	FIMS/Resources	기본 캐릭터 프리팹, FIMS 시스템 프리팹
	FIMS/Scenes		핌스 대기룸 씬
	FIMS/Shader		캐릭터, 기타 쉐이더
	FIMS/Textures	공통 UI, 앱아이콘
	
3) 스크립트구조
	Fims.Scripts.xlsx 참조