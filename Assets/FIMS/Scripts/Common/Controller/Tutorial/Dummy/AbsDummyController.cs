﻿using UnityEngine;

public abstract class AbsDummyController : SMonoBehaviour
{
    public virtual float Alpha { get; set; }
    public virtual float BackButtonPress { get; set; }
    public virtual float SystemButtonPress { get; set; }
    public virtual float Battery { get; set; }

    public virtual float TouchPointX      { get; set; }
    public virtual float TouchPointY      { get; set; }
    public virtual float TouchPointAlpha   { get; set; }
    public virtual float TouchpadAlpha  { get; set; }

    void Awake()
    {
        Initialize();
    }

    void OnValidate()
    {
        Initialize();
    }

    protected abstract void Initialize();
    public abstract void Validate();
}