﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FTFPointerTester : MonoBehaviour
{
    public void OnPointerEnter()
    {
        Debug.Log("Pointer Enter");
    }
    public void OnPointerExit()
    {
        Debug.Log("Pointer Exit");
    }
    public void OnClick()
    {
        Debug.Log("OnClick");
    }

}