﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using UnityEngine.EventSystems;

namespace FTF
{
    /// <summary>
    /// FTFLaserPointer의 값을 이용해서 오브젝트를 해당 위치로 이동시킨다.
    /// 컨트롤러 모델이나 컨트롤러에 붙어있어야할 무기 등을 지정할 때 사용한다.
    /// </summary>
    public class FTFPointerFollower : SMonoBehaviour
    {
        private void Update()
        {
            if (FTFPointerBridge.Instance != null)
            {
                transform.localPosition = FTFPointerBridge.Instance.position;
                transform.localRotation = FTFPointerBridge.Instance.rotation;
            }
        }
    }
}