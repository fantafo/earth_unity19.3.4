﻿using System;
using UnityEngine;
using System.Collections;
using Pvr_UnitySDKAPI;
using FTF;

/*Pvr_ControllerDemo를 복사해서 작성
 * 컨트롤러를 사용한다면 실행됩니다.
 * 아이커서 혹은 headsetcontroller일 경우 
 * FTFReticlePvrPointer를 사용합니다.
 */
public class FTFPvr_Controller : MonoBehaviour
{
    public GameObject HeadSetController;
    public GameObject controller0;

    public GameObject cube;

    public Transform dot;
    public Transform start;
    public Transform customDot;


    private Vector3 startSize;
    private float distance;
    
    private Ray ray;
    private GameObject currentController;

    private Transform lastHit;
    private Transform currentHit;
    
    private bool noClick;
    
    // Use this for initialization
    void Start()
    {
        startSize = dot.localScale;
        distance = Vector3.Distance(dot.position, start.position);
        ray = new Ray();
        Pvr_ControllerManager.PvrServiceStartSuccessEvent += ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent += ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent += CheckControllerStateForGoblin;
#if UNITY_EDITOR
        currentController = controller0;
#endif
        /*
        if (Pvr_UnitySDKManager.SDK.HeadDofNum == HeadDofNum.SixDof || Pvr_UnitySDKManager.SDK.HandDofNum == HandDofNum.SixDof)
        {
            if (Pvr_UnitySDKManager.SDK.trackingmode == 0 || Pvr_UnitySDKManager.SDK.trackingmode == 1)
            {
                if (cube != null)
                    cube.GetComponent<BoxCollider>().enabled = false;
            }
        }
        */
        if (Pvr_UnitySDKManager.SDK != null)
        {
            if (Pvr_UnitySDKManager.pvr_UnitySDKSensor != null)
            {
                Pvr_UnitySDKManager.pvr_UnitySDKSensor.ResetUnitySDKSensor();
            }
            else
            {
                Pvr_UnitySDKManager.SDK.pvr_UnitySDKEditor.ResetUnitySDKSensor();
            }

        }
        //ray.origin = transform.position;
    }

    void OnDestroy()
    {
        Pvr_ControllerManager.PvrServiceStartSuccessEvent -= ServiceStartSuccess;
        Pvr_ControllerManager.SetControllerStateChangedEvent -= ControllerStateListener;
        Pvr_ControllerManager.ControllerStatusChangeEvent -= CheckControllerStateForGoblin;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerInstance.main != null && PlayerInstance.main.IsRoomMaster)
            controller0.SetActive(true);
        else
            controller0.SetActive(false);


#if UNITY_EDITOR
        if (HeadSetController.activeSelf)
        {
#else
        if (!HeadSetController.activeSelf)
        {
#endif
            if (currentController != null)
            {
                //ray.direction = direction.position - transform.position;
                ray.direction = dot.position - start.position;
                ray.origin = start.position;
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    currentHit = hit.transform;

                    if (currentHit != null && lastHit != null && currentHit != lastHit)
                    {
                        if (lastHit.GetComponent<Pvr_UIGraphicRaycaster>() && lastHit.transform.gameObject.activeInHierarchy && lastHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled)
                        {
                            lastHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled = false;
                        }
                    }
                    if (currentHit != null && lastHit != null && currentHit == lastHit)
                    {
                        if (currentHit.GetComponent<Pvr_UIGraphicRaycaster>() && !currentHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled)
                        {
                            currentHit.GetComponent<Pvr_UIGraphicRaycaster>().enabled = true;

                        }
                    }
                    lastHit = hit.transform;
                    Debug.DrawLine(ray.origin, hit.point, Color.red);

                    dot.position = hit.point;

                    //지구위에 dot를 그릴때 너무 작아서 안보임. 항상 적절한 위치에 있는 customDot으로 보여주기위해
                    if (hit.transform.root.tag == "LineDraw")
                        customDot.gameObject.SetActive(true);
                    else
                    {
                        customDot.gameObject.SetActive(false);
                    }

                 

                }
                else
                {
                    dot.position = customDot.position;
                    customDot.gameObject.SetActive(true);
                    currentHit = null;
                    noClick = false;
                }

                float tempDistance = Vector3.Distance(dot.position, start.position);

                if (dot.gameObject.activeSelf)
                {
                    dot.localScale = startSize * (tempDistance / distance);
                }

            }

            if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TOUCHPAD) ||
                Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TOUCHPAD) || Input.GetMouseButtonDown(0))
            {
                if (lastHit != null && 1 << lastHit.transform.gameObject.layer == LayerMask.GetMask("Water") && currentHit != null)
                {
                    //lastHit.transform.GetComponent<Renderer>().material = clickmat;
                    noClick = true;
                }
            }
        }
        else
        {

        }
    }

   

    private void ServiceStartSuccess()
    {
        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
            Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            HeadSetController.SetActive(false);
        }
        else
        {
            HeadSetController.SetActive(true);
        }
        if (Controller.UPvr_GetMainHandNess() == 0)
        {
            currentController = controller0;
        }
    }

    private void ControllerStateListener(string data)
    {

        if (Controller.UPvr_GetControllerState(0) == ControllerState.Connected ||
            Controller.UPvr_GetControllerState(1) == ControllerState.Connected)
        {
            HeadSetController.SetActive(false);
        }
        else
        {
            HeadSetController.SetActive(true);
        }

        if (Controller.UPvr_GetMainHandNess() == 0)
        {
            currentController = controller0;
        }
    }

    private void CheckControllerStateForGoblin(string state)
    {
        HeadSetController.SetActive(!Convert.ToBoolean(Convert.ToInt16(state)));
    }

}
