﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 유저들이 대기하고 게임을 시작할 수 있는 방 정보로서,
    /// 방번호, 제목, 등의 기본 방 설정을 담고있다.
    /// 
    /// RoomInstance를 통해서 GameInstance가 생성되며 GameInstance에 접근하기 위해서는
    /// 해당 클래스를 통과해야한다.
    /// 
    /// 0번째 플레이어는 방장으로 취급한다.
    /// </summary>
    public class Data
    {
        class TempDic<T> : Dictionary<string, Entry<T>>
        {
            internal static TempDic<T> main = new TempDic<T>();
        }
        class Entry<T>
        {
            internal bool isDirty = true;
            internal bool isInit = true;
            internal T value;
            internal List<Action> onChanged;
        }

        static object LOCK = new object();
        public static bool isDirty;

        public static T Get<T>(string key, T def = default(T)) { Entry<T> result; if (TempDic<T>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }

        public static int GetInt(string key, int def = 0) { Entry<int> result; if (TempDic<int>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }
        public static float GetFloat(string key, float def = 0) { Entry<float> result; if (TempDic<float>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }
        public static bool GetBool(string key, bool def = false) { Entry<bool> result; if (TempDic<bool>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }
        public static string GetString(string key, string def = null) { Entry<string> result; if (TempDic<string>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }
        public static Vector3 GetVector3(string key) { return GetVector3(key, Vector3.zero); }
        public static Vector3 GetVector3(string key, Vector3 def) { Entry<Vector3> result; if (TempDic<Vector3>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }
        public static Color32 GetColor(string key) { return GetColor(key, Color.white); }
        public static Color GetColor(string key, Color def) { Entry<Color> result; if (TempDic<Color>.main.TryGetValue(key, out result) && result.isInit) { return result.value; } else { return def; } }

        public static void SetString(string key, string value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<string> result;
                if (TempDic<string>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<string>.main.Add(key, new Entry<string> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }
        public static void SetInt(string key, int value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<int> result;
                if (TempDic<int>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<int>.main.Add(key, new Entry<int> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }
        public static void SetFloat(string key, float value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<float> result;
                if (TempDic<float>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<float>.main.Add(key, new Entry<float> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }
        public static void SetBool(string key, bool value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<bool> result;
                if (TempDic<bool>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<bool>.main.Add(key, new Entry<bool> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }
        public static void SetVector3(string key, Vector3 value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<Vector3> result;
                if (TempDic<Vector3>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<Vector3>.main.Add(key, new Entry<Vector3> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }
        public static void SetColor(string key, Color value, bool sendPacket = true)
        {
            lock (LOCK)
            {
                Entry<Color> result;
                if (TempDic<Color>.main.TryGetValue(key, out result))
                {
                    if (value != result.value)
                    {
                        result.isDirty = true;
                        result.isInit = true;
                        result.value = value;
                    }
                }
                else
                {
                    TempDic<Color>.main.Add(key, new Entry<Color> { value = value });
                }

                if (sendPacket)
                    Networker.Send(C_Data.SetData(key, value));
            }
        }


        public static void ProcDelegate()
        {
            if (isDirty)
            {
                List<Action> callList = new List<Action>();
                collectDelegate(TempDic<string>.main, callList);
                collectDelegate(TempDic<int>.main, callList);
                collectDelegate(TempDic<float>.main, callList);
                collectDelegate(TempDic<bool>.main, callList);
                collectDelegate(TempDic<Vector3>.main, callList);
                collectDelegate(TempDic<Color>.main, callList);

                for (int i = 0; i < callList.Count; i++)
                {
                    try
                    {
                        callList[i]();
                    }
                    catch (Exception e)
                    {
                        e.PrintStackTrace();
                    }
                }
            }
        }
        private static void collectDelegate<T>(Dictionary<string, Entry<T>> dic, List<Action> callList)
        {
            var e = dic.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Current.Value.isDirty)
                {
                    var entry = e.Current.Value;
                    for (int i = 0, count = entry.onChanged.Count; i < count; i++)
                    {
                        if (!callList.Contains(entry.onChanged[i]))
                        {
                            callList.Add(entry.onChanged[i]);
                        }
                    }
                }
            }
        }

        public static void RegistCallback<T>(string key, Action callback, bool execute = true)
        {
            if (callback == null || key.IsEmpty())
                return;

            lock (LOCK)
            {
                Entry<T> result;
                if (!TempDic<T>.main.TryGetValue(key, out result))
                {
                    result = new Entry<T>()
                    {
                        isDirty = false,
                        isInit = false,
                    };
                }

                if (result.onChanged == null)
                    result.onChanged = new List<Action>();

                if (result.onChanged.Contains(callback))
                    result.onChanged.Add(callback);

                // 이미 등록된 객체라면 바로 실행에 옮긴다
                if (result.isInit)
                {
                    result.isDirty = false;
                    callback();
                }
            }
        }
        public static void UnregistCallback<T>(string key, Action callback)
        {
            lock (LOCK)
            {
                Entry<T> result;
                if (!TempDic<T>.main.TryGetValue(key, out result))
                {
                    result = new Entry<T>()
                    {
                        isDirty = false,
                        isInit = false,
                    };
                }

                if (result.onChanged != null)
                {
                    result.onChanged.Remove(callback);
                }

            }
        }

        public static void Clear()
        {
            TempDic<int>.main.Clear();
            TempDic<float>.main.Clear();
            TempDic<bool>.main.Clear();
            TempDic<string>.main.Clear();
            TempDic<Vector3>.main.Clear();
            TempDic<Color>.main.Clear();
        }

        public static bool Remove(string key)
        {
            if (TempDic<int>.main.Remove(key)) return true;
            if (TempDic<float>.main.Remove(key)) return true;
            if (TempDic<bool>.main.Remove(key)) return true;
            if (TempDic<string>.main.Remove(key)) return true;
            if (TempDic<Vector3>.main.Remove(key)) return true;
            if (TempDic<Color>.main.Remove(key)) return true;

            return false;
        }


        /*

        private void Awake()
        {
            if(_main != null)
            {
                Destroy(gameObject);
            }

            _main = this;
            DontDestroyOnLoad(gameObject);
        }

        private void LateUpdate()
        {
            ProcDelegate();
        }*/
    }
}