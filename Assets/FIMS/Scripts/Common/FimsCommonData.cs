﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;

public class FimsCommonData
{
#if UNITY_EDITOR || UNITY_STANDALONE
    public const string ROOT = "";
#else
    public const string ROOT = "/sdcard/Fantafo/";
#endif
    public const string FLAG_ROOT = ROOT + "Flags/";
    public const string FLAG_CONTROLLER = "Controller";
    public const string FLAG_LIVE = "Live";
    public const string FLAG_LOADING = "Loading";
    public const string FLAG_Alloc = "Alloc";

    public const string DEFAULT_PATH = ROOT + "default.properties";
    public const string CONNECT_PATH = ROOT + "connect.properties";
    public const string SCORE_PATH = ROOT + "score.properties";


    public static bool IsAllocated { get { return HasFlag(FLAG_Alloc); } }
    public static bool IsLoading { get { return HasFlag(FLAG_LOADING); } set { if (value) { PushFlag(FLAG_LOADING); } else { PopFlag(FLAG_LOADING); } } }

    static void Check()
    {
        if(!Directory.Exists(FLAG_ROOT))
        {
            Directory.CreateDirectory(FLAG_ROOT);
        }
    }

    public static bool HasFlag(string name)
    {
        try
        {
            return File.Exists(FLAG_ROOT + name);
        }
        catch (Exception e)
        {
            SLog.Exception(e);
        }
        return false;
    }
    public static string GetFlag(string name, string def = "")
    {
        try
        {
            string path = FLAG_ROOT + name;
            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }
        }
        catch (Exception e)
        {
            SLog.Exception(e);
        }
        return def;
    }
    public static void SetFlag(string name, string value)
    {
        try
        {
            Check();
            string path = FLAG_ROOT + name;
            File.WriteAllText(path, value, Encoding.UTF8);
            try
            {
                File.SetLastWriteTime(path, DateTime.Now);
            }
            catch (IOException) { }
        }
        catch (Exception e)
        {
            SLog.Exception(e);
        }
    }
    public static void PushFlag(string name)
    {
        SetFlag(name, "");
    }
    public static void PopFlag(string name)
    {
        try
        {
            if (File.Exists(FLAG_ROOT + name))
                File.Delete(FLAG_ROOT + name);
        }
        catch (Exception e)
        {
            SLog.Exception(e);
        }
    }
}