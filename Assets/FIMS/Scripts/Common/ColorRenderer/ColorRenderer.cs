﻿using UnityEngine;

/// <summary>
/// 카메라에 색을 입힐 수 있는 컴포넌트 입니다.
/// </summary>
[RequireComponent(typeof(Camera))]
public class ColorRenderer : MonoBehaviour
{
    /********************************************************************************/
    /*                                                                              */
    /*                               Variables                                      */
    /*                                                                              */
    /********************************************************************************/

    public Material _colorMaterial;

    /********************************************************************************/
    /*                                                                              */
    /*                             Life Cycles                                      */
    /*                                                                              */
    /********************************************************************************/

    void Start()
    {
        if (_colorMaterial == null)
        {
            _colorMaterial = new Material(Shader.Find("Sprites/Default"));
            _colorMaterial.color = Color.white;
        }
    }

    void OnPostRender()
    {
        if (_colorMaterial == null)
            return;

        _colorMaterial.SetPass(0);

        GL.PushMatrix();
        GL.LoadOrtho();
        GL.Color(_colorMaterial.color);
        GL.Begin(GL.QUADS);
        GL.Vertex3(0f, 0f, -12f);
        GL.Vertex3(0f, 1f, -12f);
        GL.Vertex3(1f, 1f, -12f);
        GL.Vertex3(1f, 0f, -12f);
        GL.End();
        GL.PopMatrix();
    }
}