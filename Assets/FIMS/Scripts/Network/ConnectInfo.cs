﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class ConnectInfo : SMonoBehaviour
{
    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Static Variables                   //
    //                                                    //
    ////////////////////////////////////////////////////////

    static ConnectInfo main;
    static Properties prop = new Properties();
    [System.Diagnostics.Conditional("UNITY_EDITOR")]
    static void Check() { if (main == null) throw new NullReferenceException("ConnectInfo가 로딩되지 않았습니다."); }


    // user
    public static long UserSN { get { return prop.GetLong("user.sn", main.sn); } }
    public static string UserName { get { return prop.GetString("user.name", main.userName); } }

    // client
    public static string ConnectIP { get { return prop.GetString("client.connect.ip", main.connectIP); } }
    public static int ConnectPort { get { return prop.GetInt("client.connect.port", main.connectPort); } }

    // room
    public static int RoomID { get { return prop.GetInt("room.id", main.roomID); } }
    public static string RoomName { get { return prop.GetString("room.name", main.roomName); } }
    public static string RoomPassword { get { return prop.GetString("room.password", main.roomPassword); } }
    public static int MapID { get { return prop.GetInt("room.map", main.roomMapID); } }
    public static int UserCount { get { return prop.GetInt("room.user.count", main.roomUserCount); } }

    public static string Identifier { get { return prop.GetString("identifier", Application.identifier); } }



    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Member Variables                   //
    //                                                    //
    ////////////////////////////////////////////////////////

    [Header("User")]
    public long sn = 0;
    public string userName = null;

    [Header("Client")]
    public string connectIP = "127.0.0.1";
    public int connectPort = 40000;

    [Header("Room")]
    public int roomID = -1;
    public int roomMapID = 1;
    public string roomName = "-";
    public string roomPassword = "-";
    public int roomUserCount = 1;

    private string WebIP;
    private string PackageName;

    ////////////////////////////////////////////////////////
    //                                                    //
    //                 Life Cycles                        //
    //                                                    //
    ////////////////////////////////////////////////////////

    private void Awake()
    {
        if (main)
        {
            gameObject.Destroy();
            return;
        }

        main = this;
        prop.Clear();

        userName = FimsCommonData.GetFlag("Name", null) ?? userName;

        if (File.Exists(FimsCommonData.DEFAULT_PATH))
        {
            prop.Load(FimsCommonData.DEFAULT_PATH);
        }
        if (File.Exists(FimsCommonData.CONNECT_PATH))
        {
            prop.Load(FimsCommonData.CONNECT_PATH);
        }

#if FTF_OBSERVER
        ReadInfo();
        //commonserver ip port 설정
        StartCoroutine(GetIPAddress(WebIP));
#endif
    }

    private void OnDestroy()
    {
        if (main == this)
        {
            main = null;
            prop.Clear();
        }
    }

    IEnumerator GetIPAddress(string webserverIP)
    {
        //UnityWebRequest www = UnityWebRequest.Get("http://172.30.1.40/api/game/GetIPAddress?pkgName=com.fantafo.earth");
        UnityWebRequest www = UnityWebRequest.Get("http://" + webserverIP + "/api/game/GetIPAddress?pkgName=" + PackageName);

        Debug.Log("http://" + webserverIP + "/api/game/GetIPAddress?pkgName=" + PackageName);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Or retrieve results as binary data
            string results = Encoding.Default.GetString(www.downloadHandler.data).Replace("\"", "");
            Debug.Log("results : " + results);
            if (!results.IsEmpty() && results.Contains(":"))
            {
                string[] ipadress = results.Split(':');
                connectIP = ipadress[0];
                connectPort = Int32.Parse(ipadress[1]);
                Debug.Log("connectPort" + connectPort);
            }
        }
    }


    private void ReadInfo()
    {
        string[] textString = System.IO.File.ReadAllLines("connectinfo.txt");
        WebIP = textString[0];
        PackageName = textString[1];
        Debug.Log("WEB IP : " + WebIP);
        Debug.Log("PKG NAME : " + PackageName);

    }
}