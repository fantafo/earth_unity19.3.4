﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// ObserverMain에서 AI가 처리하는 방향대로 서버에서 정확한 응답을 줬는지에 대해 검사하는 역할을 한다.
    /// </summary>
    public class ObserverPacketHandler : NetworkerPacketHandler
    {
        public ObserverMain observer;
        private long currentRoomID = -1;

        void Next(object value = null)
        {
            observer.scheduler.Next(code, value);
        }
        void Break()
        {
            observer.scheduler.Break(code);
        }


        protected override void OnServerVersion()
        {
            base.OnServerVersion();
            Next();
        }

        protected override void OnLoginResult()
        {
            base.OnLoginResult();
            Next();
        }

        protected override void OnRoomList()
        {
            using (PList<long> roomList = PListPool<long>.Take())
            {
                int length = reader.ReadD();
                for (int i = 0; i < length; i++)
                {
                    long num = reader.ReadL();
                    string name = reader.ReadS();
                    short mapID = reader.ReadH();
                    byte playerCnt = reader.ReadC();
                    byte maxCnt = reader.ReadC();
                    bool started = reader.ReadB();

                    // 현재 접속하고 있는 방이 아닐경우
                    if (started && num != currentRoomID)
                    {
                        roomList.Add(num);
                    }
                }

                if (roomList.Count > 0)
                {
                    Next(roomList.Random());
                }
                else
                {
                    Break();
                }
            }
        }

        protected override void OnRoomInfo()
        {
            base.OnRoomInfo();
            currentRoomID = RoomInfo.main.num;
            Next();
        }

        protected override void OnSceneStart()
        {
            base.OnSceneStart();
            Next();
        }
    }
}