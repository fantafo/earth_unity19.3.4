﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FTF.Packet
{
    public class C_Data
    {
        public enum TYPE
        {
            String,
            Int,
            Float,
            Bool,
            Vector3,
            Color
        }

        ///////////////////////////////////////////////////////////////////
        /// 
        /// 
        /// 
        ///////////////////////////////////////////////////////////////////
        /*
        public static byte[] SetData<T>(string key, T value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);

            if (value is string)
            {
                w.WriteC((int)TYPE.String);
                w.WriteS(key);
                w.WriteS(value as string);
            }
            else if (value is int)
            {
                w.WriteC((int)TYPE.Int);
                w.WriteS(key);
                w.WriteD(Convert.ToInt32(value));
            }
            else if (value is float)
            {
                w.WriteC((int)TYPE.Float);
                w.WriteS(key);
                w.WriteF(Convert.ToSingle(value));
            }
            else if (value is bool)
            {
                w.WriteC((int)TYPE.Bool);
                w.WriteS(key);
                w.WriteB(Convert.ToBoolean(value));
            }
            else if (value is Vector3)
            {
                w.WriteC((int)TYPE.Vector3);
                w.WriteS(key);
                w.WriteV3((Vector3)(object)value);
            }
            else if (value is Color32 || value is Color)
            {
                w.WriteC((int)TYPE.Color);
                w.WriteS(key);
                w.WriteS(value as string);
            }
            return w.ToBytes();
        }*/
        public static byte[] SetData(string key, string value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.String);
            w.WriteS(key);
            w.WriteS(value);
            return w.ToBytes();
        }
        public static byte[] SetData(string key, int value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.Int);
            w.WriteS(key);
            w.WriteD(value);
            return w.ToBytes();
        }
        public static byte[] SetData(string key, float value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.Float);
            w.WriteS(key);
            w.WriteF(value);
            return w.ToBytes();
        }
        public static byte[] SetData(string key, bool value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.Bool);
            w.WriteS(key);
            w.WriteB(value);
            return w.ToBytes();
        }
        public static byte[] SetData(string key, Vector3 value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.Vector3);
            w.WriteS(key);
            w.WriteV3(value);
            return w.ToBytes();
        }
        public static byte[] SetData(string key, Color32 value)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Scene_SetData);
            w.WriteC((int)TYPE.Color);
            w.WriteS(key);
            w.WriteRGBA(value);
            return w.ToBytes();
        }
    }
}