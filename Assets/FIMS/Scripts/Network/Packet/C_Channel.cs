﻿namespace FTF.Packet
{
    /// <summary>
    /// 채널 내에서 방 목록, 생성, 입장 등의 패킷 모음
    /// </summary>
    public class C_Channel
    {
        public static byte[] RequestList()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Channel_RequestRoomList);
            return w.ToBytes();
        }

        public static byte[] Create(string name, string password, short mapID, byte maxUser, bool autoStart)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Channel_CreateRoom);
            w.WriteS(name);
            w.WriteS(password);
            w.WriteH(mapID);
            w.WriteC(maxUser);
            w.WriteB(autoStart);
            return w.ToBytes();
        }

        public static byte[] Join(long roomID, string password, bool autoCreate = true)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ClientOpcode.Channel_JoinRoom);
            w.WriteL(roomID);
            w.WriteS(password);
            w.WriteB(autoCreate);

            return w.ToBytes();
        }
    }
}