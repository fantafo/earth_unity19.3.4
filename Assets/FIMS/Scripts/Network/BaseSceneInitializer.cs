﻿using FTF.Packet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 방이 생성됐을 때, 방장이 전체 유저에게 방을 세팅해줄때 사용된다.
    /// 처음 방이 생성돼서 방에 접속하면 방장에게 InitalizeTo 메서드가 호출되고,
    /// 파라미터로 들어온 플레이어에게 초반세팅을 해주어야한다.
    /// 이것은 최초 방 생성 뿐 아니라, 중간에 난입하거나 재접속한 유저에게도 해당된다.
    /// 만약 방장이 바뀌거나 접속이 해제되면 다음 유저가 방장 대신 일을 일임하여 실행된다.
    /// 즉 모든 유저가 방장이 될 수 있다는 가정하에 데이터를 가지고 있어야한다.
    /// </summary>
    public abstract class BaseSceneInitializer : SMonoBehaviour
    {
        public static BaseSceneInitializer main;

        protected virtual void Awake()
        {
            if (main)
                gameObject.Destroy();

            main = this;
        }

        public abstract void InitializeTo(PlayerInstance player);
    }
}