﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF
{
    /// <summary>
    /// 컨트롤러의 키 입력가능 여부를 나타내주는 유틸컴포넌트다.
    /// Push를 하여 특정 오브젝트에게 현재의 권한을 주게되며,
    /// Pop을 통해서 권한을 삭제한다.
    /// 
    /// Is를 통해 현재 오브젝트가 권한을 갖고있는지를 체크할 수 있도록 한다.
    /// 
    /// 이것은 전체적인 시스템에 탑제돼있지 않고,
    /// 프로그래머가 선택적으로 사용하게끔 돼 있다.
    /// 
    /// Earth 프로젝트의 ActiveForCountry 코드를 확인해보기바란다.
    /// </summary>
    public class KeyStack
    {
        static List<object> list = new List<object>();
        public static void Push(object obj)
        {
            list.Add(obj);
            Check();
        }
        public static void Pop(object obj)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == obj)
                {
                    list.RemoveAt(i--);
                }
            }
            Check();
        }
        public static bool Is(object obj)
        {
            return list.Count > 0 && list[list.Count - 1] == obj;
        }
        public static bool IsNone
        {
            get
            {
                return list.Count == 0;
            }
        }

        public static void Clear()
        {
            list.Clear();
        }

        public static bool Contains(object obj)
        {
            return list.Contains(obj);
        }
        static void Check()
        {
            for(int i=0; i<list.Count; i++)
            {
                var uobj = list[i] as UnityEngine.Object;
                if(list[i] == null || !(uobj))
                {
                    list.RemoveAt(i--);
                }
            }
        }
    }
}