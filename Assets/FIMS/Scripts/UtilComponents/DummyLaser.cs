﻿// Copyright 2017 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using FTF;
using System;
using UnityEngine;
using UnityEngine.Assertions;

/// Visualizes a laser and a reticle using a LineRenderer and a Quad.
/// Provides functions for settings the end point of the laser,
/// and clamps the laser and reticle based on max distances.
[RequireComponent(typeof(LineRenderer))]
public class DummyLaser : SMonoBehaviour
{
    public PlayerInfo info;

    /// Final size of the reticle in meters when it is 1 meter from the camera.
    /// The reticle will be scaled based on the size of the mesh so that it's size
    /// matches this size.
    public const float RETICLE_SIZE_METERS = 0.2f;

    [Tooltip("Color of the laser pointer including alpha transparency")]
    public Color laserColor = new Color(1.0f, 1.0f, 1.0f, 0.25f);

    [Tooltip("Color of the laser pointer including alpha transparency")]
    public Color laserColorEnd = new Color(1.0f, 1.0f, 1.0f, 0.0f);
    
    [Tooltip("References the reticle that will be positioned.")]
    [SerializeField]
    private Transform reticle;

    private const float LERP_CLAMP_THRESHOLD = 0.02f;
    
    public Transform Reticle
    {
        get
        {
            return reticle;
        }
        set
        {
            reticle = value;
            ReticleMeshSizeMeters = 1.0f;
            ReticleMeshSizeRatio = 1.0f;

            if (reticle != null)
            {
                MeshFilter meshFilter = reticle.GetComponent<MeshFilter>();
                if (meshFilter != null && meshFilter.mesh != null)
                {
                    ReticleMeshSizeMeters = meshFilter.mesh.bounds.size.x;
                    if (ReticleMeshSizeMeters != 0.0f)
                    {
                        ReticleMeshSizeRatio = 1.0f / ReticleMeshSizeMeters;
                    }
                }
            }
        }
    }

    [Range(-32767, 32767)]
    public int reticleSortingOrder = 0;

    /// The size of the reticle's mesh in meters.
    public float ReticleMeshSizeMeters { get; private set; }

    /// The ratio of the reticleMeshSizeMeters to 1 meter.
    /// If reticleMeshSizeMeters is 10, then reticleMeshSizeRatio is 0.1.
    public float ReticleMeshSizeRatio { get; private set; }

    /// Reference to the laser's line renderer.
    public LineRenderer Laser { get; private set; }

    /// Optional delegate for customizing how the currentPosition is calculated based on the distance.
    /// If not set, the currentPosition is determined based on the distance multiplied by the forward
    /// direction of the transform added to the position of the transform.
    public delegate Vector3 GetPointForDistanceDelegate(float distance);
    public GetPointForDistanceDelegate GetPointForDistanceFunction { get; set; }

    //private float targetDistance;
    //private float currentDistance;
    private Vector3 currentPosition;
    //private Vector3 currentLocalPosition;
    //private Quaternion currentLocalRotation;

    void Awake()
    {
        Laser = GetComponent<LineRenderer>();

        // Will calculate reticleMeshSizeMeters and reticleMeshSizeRatio
        Reticle = reticle;

        // Set the reticle sorting order.
        if (reticle != null)
        {
            Renderer reticleRenderer = reticle.GetComponent<Renderer>();
            Assert.IsNotNull(reticleRenderer);
            reticleRenderer.sortingOrder = reticleSortingOrder;
        }
    }

    private void OnEnable()
    {
        if(info != null)
        {
            currentPosition = info.model.reticlePosition;
        }
    }

    void LateUpdate()
    {
        if(info != null)
        {
            UpdateCurrentPosition();
            UpdateReticlePosition();
            UpdateLaserAlpha();
        }
    }

    void OnWillRenderObject()
    {
        Camera camera = Camera.current;
        UpdateReticleSize(camera);
        UpdateReticleOrientation(camera);
    }

    private void UpdateCurrentPosition()
    {
        if (info != null)
        {
            float ratio = Time.deltaTime * info.model.speed;
            if(info.IsMainPlayer && !PlayerInstance.IsObserver)
            {
                ratio = 1;
            }

            currentPosition = Vector3.Lerp(currentPosition, info.model.reticlePosition, ratio);
        }
    }

    private void UpdateReticlePosition()
    {
        if (reticle == null)
        {
            return;
        }

        Laser.useWorldSpace = true;
        Laser.SetPosition(0, transform.position);
        Laser.SetPosition(1, currentPosition);

        reticle.position = currentPosition;
    }

    private void UpdateLaserAlpha()
    {
        float alpha = 1.0f;
#if UNITY_5_6_OR_NEWER
        Laser.startColor = Color.Lerp(Color.clear, laserColor, alpha);
        Laser.endColor = laserColorEnd;
#else
    Laser.SetColors(Color.Lerp(Color.clear, laserColor, alpha), laserColorEnd);
#endif  // UNITY_5_6_OR_NEWER
    }

    private void UpdateReticleSize(Camera camera)
    {
        if (reticle == null)
        {
            return;
        }

        if (camera == null)
        {
            return;
        }

        float reticleDistanceFromCamera = (reticle.position - camera.transform.position).magnitude;
        float scale = RETICLE_SIZE_METERS * ReticleMeshSizeRatio * reticleDistanceFromCamera;
        reticle.localScale = new Vector3(scale, scale, scale);
    }

    private void UpdateReticleOrientation(Camera camera)
    {
        if (reticle == null)
        {
            return;
        }

        if (camera == null)
        {
            return;
        }

        Vector3 direction = reticle.position - camera.transform.position;
        reticle.rotation = Quaternion.LookRotation(direction, Vector3.up);

        //if (doesReticleFaceCamera.IsAnyAxisOff)
        //{
        //    Vector3 euler = reticle.localEulerAngles;
        //    if (!doesReticleFaceCamera.alongXAxis)
        //    {
        //        euler.x = 0.0f;
        //    }

        //    if (!doesReticleFaceCamera.alongYAxis)
        //    {
        //        euler.y = 0.0f;
        //    }

        //    if (!doesReticleFaceCamera.alongZAxis)
        //    {
        //        euler.z = 0.0f;
        //    }

        //    reticle.localEulerAngles = euler;
        //}
    }

    void OnValidate()
    {
        // If the "reticle" serialized field is changed while the application is playing
        // by using the inspector in the editor, then we need to call the Reticle setter to
        // ensure that ReticleMeshSizeMeters and ReticleMeshSizeRatio are updated.
        // Outside of the editor, this can't happen because only the Reticle setter is publicly
        // accessible.  The Laser null check excludes cases where OnValidate is invoked before Awake.
        if (Application.isPlaying && Laser != null)
        {
            Reticle = reticle;
        }
    }
}