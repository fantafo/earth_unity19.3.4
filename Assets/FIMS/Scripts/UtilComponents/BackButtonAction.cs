﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// 컨트롤러가 아닌 해드트래킹 인터렉션을 할때 (FTFReticlePointer를 사용할 떄)
    /// BackButtonAction.Action()을 사용하게되면 컨트롤러에서 Back버튼을 누른것과 같이 처리해준다.
    /// </summary>
    public class BackButtonAction : SMonoBehaviour
    {
        public void Action()
        {
            StartCoroutine(BackbuttonClick());
        }

        IEnumerator BackbuttonClick()
        {
            yield return Waits.EndOfFrame;
            VRInput.GooglePlatform.ReticleBackButton = true;
            yield return Waits.EndOfFrame;
            VRInput.GooglePlatform.ReticleBackButton = false;
        }
    }
}