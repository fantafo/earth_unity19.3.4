﻿using UnityEngine;
using System.Collections;
using FTF;

[Description("플레이어가 메인 플레이어인 경우 activeTarget를 키고, 메인플레이어가 아닌경우 끈다. deactiveTargets의 경우 메인 플레이어인 경우 모두 끄고, 메인플레이어가 아닌경우 킨다")] 
public class CullMainPlayer : SMonoBehaviour
{
    public PlayerInfo info;
    public GameObject[] activeTargets;
    public GameObject[] deactiveTargets;
    public bool inverse;

    PlayerInstance beforeInstance;
    PlayerInstance beforeMainPlayerInstance;

    private void Reset()
    {
        info = GetComponentInParent<PlayerInfo>();
    }

    void OnEnable()
    {
        if (info == null)
        {
            enabled = false;
            return;
        }

        Setup();
    }

    private void OnValidate()
    {
        Setup();
    }

    private void Update()
    {
        if (info.instance != beforeInstance || beforeMainPlayerInstance != PlayerInstance.lookMain)
        {
            Setup();
        }
    }

    void Setup()
    {
        bool active = (!inverse == (info != null && info.IsMainPlayer && !PlayerInstance.IsObserver));

        beforeInstance = info.instance;
        beforeMainPlayerInstance = PlayerInstance.lookMain;

        for (int i = 0; i < activeTargets.Length; i++)
        {
            activeTargets[i].SetActive(active);
        }
        for (int i = 0; i < deactiveTargets.Length; i++)
        {
            deactiveTargets[i].SetActive(!active);
        }
    }
}