﻿using System;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 지속적인 서버 데이터 읽기에 따른 byte배열 가비지 최소화를 위하여 풀을 제작하여 운영한다.
/// </summary>
public static class BytePool
{
    static List<Stack<byte[]>> s_pools;

#if UNITY_EDITOR
    static int SIZE;
#endif

    static BytePool()
    {
        Initialize();
    }

    [RuntimeInitializeOnLoadMethod]
    public static void Initialize()
    {
        if (s_pools != null)
            return;

        s_pools = new List<Stack<byte[]>>();
        ;
        for (int i = 1; i < 0xFF; i++)
        {
            PrepareSize(i * 4, 0x7FF / i);
        }
#if UNITY_EDITOR
        Debug.LogFormat("[BytePool] Initialize {0:n0} Byte", SIZE);
#endif
    }

    public static void PrepareSize(int size, int count)
    {
        if (size <= 0)
            throw new Exception("Array Size는 1 이상이여야합니다. :" + size);

        // 4의 배수로 사이즈가 지정된다.
        int index = IndexOf(size);
        int arraySize = SizeOf(index);

        // 없는 부분만큼 생성해준다.
        for (int i = s_pools.Count; i <= index; i++)
            s_pools.Add(new Stack<byte[]>());

        // 풀에 추가한다.
        Stack<byte[]> pool = s_pools[index];
        for (int i = 0; i < count; i++)
        {
            pool.Push(new byte[arraySize]);
#if UNITY_EDITOR
            SIZE += arraySize;
#endif
        }
    }

    static int IndexOf(int size)
    {
        return ((size - 1) / 4);
    }
    static int SizeOf(int index)
    {
        return (index * 4) + 4;
    }

    public static byte[] Take(int size)
    {
        lock (s_pools)
        {
            if (size <= 0)
                throw new Exception("Array Size는 1 이상이여야합니다. :" + size);

            int index = IndexOf(size);
            if (index < s_pools.Count)
            {
                if (s_pools[index].Count > 0)
                {
                    return s_pools[index].Pop();
                }
                else
                {
#if UNITY_EDITOR
                    Debug.LogWarning("[BytePool] " + SizeOf(index) + "의 배열이 부족해서 새로 생성됐습니다.");
#endif
                    return new byte[SizeOf(index)];
                }
            }
            else
            {
#if UNITY_EDITOR
                Debug.LogWarning("[BytePool] " + SizeOf(index) + "이 새로운 배열사이즈로서 생성됐습니다.");
#endif
                return new byte[SizeOf(index)];
            }
        }
    }
    public static void Release(byte[] array)
    {
        lock (s_pools)
        {
            if ((array == null))
                //|| ((array.Length % 4) != 0))
            {
                throw new Exception("반환된 Array가 Null이거나 길이가 정상적이지 않습니다.");
            }

            int index = IndexOf(array.Length);
#if UNITY_EDITOR
            if (index >= s_pools.Count)
            {
                Debug.LogWarning("[BytePool] " + array.Length + "가 새로운 풀에 등록됐습니다. 풀 설정을 다시 해 주십시오.");
            }
#endif
            for (int i = s_pools.Count; i <= index; i++)
                s_pools.Add(new Stack<byte[]>());

            s_pools[index].Push(array);
        }
    }
}