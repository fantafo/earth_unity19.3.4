﻿using UnityEngine;

/// <summary>
/// 패키지명을 읽어서 저장하고 있습니다.
/// 메인쓰레드 외의 쓰레드에서 사용하기 위해 제작됐습니다.
/// </summary>
public class PackageNameHelper : SMonoBehaviour
{
    public static string PackageName;

    public string _packageName;

#if UNITY_EDITOR
    private void Reset()
    {
#if UNITY_5_6_OR_NEWER
        _packageName = UnityEditor.PlayerSettings.applicationIdentifier;
#else
            _packageName = UnityEditor.PlayerSettings.bundleIdentifier;
#endif
    }
#endif

    private void Awake()
    {
        PackageName = _packageName;
    }
}