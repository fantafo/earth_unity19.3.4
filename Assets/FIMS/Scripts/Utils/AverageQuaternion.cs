﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 입력된 Quaternion 값의 평균값을 구해주는 컴포넌트입니다.
/// 단순한 평균을 구하는 것은 아니며, 이전에 입력된 값들을 저장하고있습니다.
/// 
/// Capacity를 초과하는 값들은 과거에 들어온 순선대로 삭제되며
/// Average는 최신 데이터들로만  유지되어 평균을 구할 수 있습니다.
/// </summary>
public class AverageQuaternion
{
    Quaternion[] queue;
    int index;
    int length;

    public Quaternion last
    {
        get
        {
            if (length == 0)
                return Quaternion.identity;
            return queue[(index + queue.Length - 1) % queue.Length];
        }
    }

    public AverageQuaternion(int count)
    {
        queue = new Quaternion[count];
        Reset();
    }

    public void Push(Quaternion quart)
    {
        queue[index++] = quart;

        length = Mathf.Max(index, length);
        if (index == queue.Length)
            index = 0;
    }

    public Quaternion Average()
    {
        Quaternion avrg = Quaternion.identity;
        int len = (queue.Length == length) ? length : index;
        float ratio = 1f / len;

        for (int i = 0; i < len; i++)
        {
            avrg *= Quaternion.Lerp(Quaternion.identity, queue[i], ratio);
        }

        return avrg;
    }

    public void Reset()
    {
        index = 0;
        length = 0;
    }
    public void Set(Quaternion val)
    {
        Reset();
        Push(val);
    }


    public static AverageQuaternion operator +(AverageQuaternion av3, Quaternion val)
    {
        av3.Push(val);
        return av3;
    }
    public static implicit operator Quaternion(AverageQuaternion av3)
    {
        return av3.Average();
    }
}