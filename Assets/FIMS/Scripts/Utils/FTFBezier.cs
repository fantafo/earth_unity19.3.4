﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 왜 이걸 만들었는지 기억이 안납니다.
    /// 뭐.. 어쨋던
    /// LTBezier를 수정한 판이라는 것만 알아주시길 바랍니다.
    /// </summary>
    public class FTFBezier
    {
        public float length;

        public Vector3 p1, p2, c1, c2;

        private Vector3 a;
        private Vector3 aa;
        private Vector3 bb;
        private Vector3 cc;
        private float len;
        private float[] arcLengths;

        public FTFBezier() { }
        public FTFBezier(Vector3 _p1, Vector3 _c1, Vector3 _c2, Vector3 _p2, float precision = 0.05f)
        {
            Set(_p1, _c1, _c2, _p2, precision);
        }

        public void Set(Vector3 _p1, Vector3 _c1, Vector3 _c2, Vector3 _p2, float precision = 0.05f)
        {
            p1 = _p1;
            p2 = _p2;
            c1 = _c1;
            c2 = _c2;

            this.a = _p1;
            aa = (-_p1 + 3 * (_c1 - _c2) + _p2);
            bb = 3 * (_p1 + _c2) - 6 * _c1;
            cc = 3 * (_c1 - _p1);

            this.len = 1.0f / precision;
            int newLen = (int)this.len + (int)1;
            if (arcLengths == null || arcLengths.Length != newLen)
                arcLengths = new float[newLen];
            arcLengths[0] = 0;

            Vector3 ov = _p1;
            Vector3 v;
            float clen = 0.0f;
            for (int i = 1; i <= this.len; i++)
            {
                v = bezierPoint(i * precision);
                clen += (ov - v).magnitude;
                this.arcLengths[i] = clen;
                ov = v;
            }
            this.length = clen;
        }

        private float map(float u)
        {
            float targetLength = u * this.arcLengths[(int)this.len];
            int low = 0;
            int high = (int)this.len;
            int index = 0;
            while (low < high)
            {
                index = low + ((int)((high - low) / 2.0f) | 0);
                if (this.arcLengths[index] < targetLength)
                {
                    low = index + 1;
                }
                else
                {
                    high = index;
                }
            }
            if (this.arcLengths[index] > targetLength)
                index--;
            if (index < 0)
                index = 0;

            if (u == 1)
                return 1f;
            else if (u == 0)
                return 0f;
            else
            {
                float aa = (index + (targetLength - arcLengths[index]) / (arcLengths[index + 1] - arcLengths[index])) / this.len;
                return aa;
            }
        }

        private Vector3 bezierPoint(float t)
        {
            return ((aa * t + (bb)) * t + cc) * t + a;
        }

        public Vector3 point(float t)
        {
            return bezierPoint(map(t));
        }

        public void drawGizmos()
        {
            Gizmos.color = Color.white;
            int len = (int)(length * 10);
            for (int i = 0; i < (int)len; i++)
            {
                Vector3 prev = point((i) / (float)len);
                Vector3 next = point((i + 1) / (float)len);

                Gizmos.DrawLine(prev, next);
            }

            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(p1, Vector3.one * 0.01f);
            Gizmos.DrawLine(p1, c1);

            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(p2, Vector3.one * 0.01f);
            Gizmos.DrawLine(p2, c2);
        }

        public override string ToString()
        {
            return string.Format("BezierCurve(p1:{0:f3} / c1:{1:f3} / c2:{2:f3} / p2:{3:f3})", p1, c1, c2, p2);
        }
    }
}