﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 입력된 Vector 값의 평균값을 구해주는 컴포넌트입니다.
/// 단순한 평균을 구하는 것은 아니며, 이전에 입력된 값들을 저장하고있습니다.
/// 
/// Capacity를 초과하는 값들은 과거에 들어온 순선대로 삭제되며
/// Average는 최신 데이터들로만  유지되어 평균을 구할 수 있습니다.
/// </summary>
public class AverageVector3
{
    Vector3[] queue;
    int index;
    int length;

    public AverageVector3(int count)
    {
        queue = new Vector3[count];
        Reset();
    }

    public void Push(Vector3 vect)
    {
        queue[index++] = vect;

        length = Mathf.Max(index, length);
        if (index == queue.Length)
            index = 0;
    }

    public Vector3 Average()
    {
        Vector3 avrg = Vector3.zero;
        int len = (queue.Length == length) ? length : index;

        for(int i=0; i<len; i++)
            avrg += queue[i];

        return avrg / len;
    }

    public void Reset()
    {
        index = 0;
        length = 0;
    }
    public void Set(Vector3 val)
    {
        Reset();
        Push(val);
    }


    public static AverageVector3 operator +(AverageVector3 av3, Vector3 val)
    {
        av3.Push(val);
        return av3;
    }
    public static implicit operator Vector3(AverageVector3 av3)
    {
        return av3.Average();
    }
}