﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FTF
{
    [RequireComponent(typeof(RoomInfo))]
    public class RoomLoadSceneName : RoomLoadBase
    {
        public string[] scenes;

        protected override AsyncOperation LoadAsync()
        {
            FimsCommonData.PushFlag(FimsCommonData.FLAG_LOADING);
            return SceneManager.LoadSceneAsync(scenes[room.mapID - 1], LoadSceneMode.Single);
        }

        protected override bool LoadValidate()
        {
            if (room.mapID - 1 < scenes.Length && !scenes[room.mapID - 1].IsEmpty())
            {
                return true;
            }
            SLog.ErrorFormat("RoomLoadScene", "{0}번의 씬번호가 없거나 입력할 수 없습니다. (전체등록 씬 {1}개)", room.mapID, scenes.Length);
            return false;
        }
    }
}