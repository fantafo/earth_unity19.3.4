﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class PlayerInfoRankDisplay : MonoBehaviour
{
    public PlayerInfo info;
    public GameObject nameTxt;
    private GameObject Earth;

    int test = 0;

    private void OnEnable()
    {
        Earth = GameObject.Find("WorldMap");
        StartCoroutine(ChangeChecker());
    }

    private IEnumerator ChangeChecker()
    {
        Text text = GetComponent<Text>();
        // 지구가 비활성화 될 때까지 대기
        while (true)
        {
            if (Earth.activeInHierarchy == false)
                break;
            yield return Waits.Wait(1);
        }

        nameTxt.transform.Translate(new Vector3(0, -0.0346f, 0));


        while (true)
        {

            string userRank = (info != null && info.instance != null) ?
                info.instance.ranking.ToString() + "등" : "0등";

            if (userRank != text.text)
            {
                text.text =  info.instance.ranking.ToString() + "등";
            }
            yield return Waits.Wait(1);
        }
    }
}