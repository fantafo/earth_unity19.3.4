﻿using System.IO;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 플레이어가 Unity에서 보여질 정보를 관리한다.
    /// PlayerInstance를 기반으로하여 유저의 모양이나
    /// 유저의 행동을 모사한다.
    /// </summary>
    public class PlayerInfo : SMonoBehaviour
    {
        public PlayerInstance instance;
        public PlayerModel model;
#if UNITY_EDITOR
        public bool visibleModels;
#endif

        public bool IsMainPlayer { get { return instance == PlayerInstance.lookMain; } }

        private void Awake()
        {
            if (instance == null)
                gameObject.SetActive(false);
        }

        private void Update()
        {
            if(instance == null)
            {
                gameObject.SetActive(false);
                return;
            }

            if(model != null)
            {
#if UNITY_EDITOR
                if (IsMainPlayer && !visibleModels)
#else
                if (IsMainPlayer)
#endif
                {
                    model.HeadModelType = 0;
                    model.BodyModelType = 0;
                }
                else
                {
                    model.HeadModelType = instance.headType;
                    model.BodyModelType = instance.bodyType;
                }
            }
        }
    }
}