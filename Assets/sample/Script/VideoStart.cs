﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;
using System.Threading.Tasks;
using System.Threading;
using UnityEngine.Video;

namespace FTF.Sample
{
    public class VideoStart : AutoCommander
    {

        // 360영상을 재생시키고 싱크를 맞추는 간단한 스크립트 입니다.
        // 선생님은 시작시 VideoStartCommand를 broadcast하여 전체 유저들의 영상을 시작시키게 됩니다.
        // 선생님은 주기적으로 본인의 영상 frame을 전송하고 학생들은 선생님의 frame과 본인의 frame이 어느정도 나는 경우 싱크를 조정합니다.
        // 이 스크립트는 예시를 위한 스크립트입니다. 실제 사용시 무조건 선생님 기기의 영상 frame에 맞추는 경우 선생님 기기의 영상이 멈추는 경우 모든 학생의 영상이 멈추게 됩니다.


        public VideoPlayer mainVideo;
        public long secondsTest = 0;

        private long beforeSceonds = 0;
        private const int interval = 3;

        private float seconds;

        protected override void OnEnable()                  // autocommader의 가상함수를 상속받아 만들어야 정상 작동 합니다.
        {
            base.OnEnable();
            seconds = 0;

            StartCoroutine(startWait(3f));                  // 시작 3초 후 영상 시작 명령을 보내줌

            if (PlayerInstance.main.IsRoomMaster)           // 만약 선생님 기기인 경우
            {
                StartCoroutine(FrameSender());               // 주기적으로 본인의 frame을 보내줌
            }
        }


       

        [OnCommand]
        public void VideoStartCommand()
        {
            Debug.Log("걸렸나?");
            mainVideo.Play();
        }

        [OnCommand]
        public void SendNowFrame(long second)
        {
            if (!PlayerInstance.main.IsRoomMaster)
            {
                if (Mathf.Abs(mainVideo.frame - second) > mainVideo.frameRate * interval)
                {
                    mainVideo.frame = (long)second;
                }
            }
        }

        IEnumerator FrameSender()
        {
            while (true)
            {
                yield return new WaitForSeconds(interval);
                Broadcast("SendNowFrame", mainVideo.frame);
            }
        }

        IEnumerator startWait(float seconds)
        {
            yield return new WaitForSeconds(seconds);
            Debug.Log("VideoStartCommand call");
            Broadcast("VideoStartCommand");
        }

    }
}
