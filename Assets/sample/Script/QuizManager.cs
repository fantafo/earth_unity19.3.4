﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FTF;
using FTF.Packet;

public class QuizManager : AutoCommander
{
    public Text[] toggleCount;
    public Toggle[] quizSelect;
    public int answer;

    public Canvas QuizCanvas;

    public Text Timer;
    private Coroutine timerCor;

    protected override void OnEnable()
    {
        base.OnEnable();
        QuizCanvas.GetComponent<Pvr_UICanvas>().enabled = false;
        //QuizCanvas.GetComponent<GvrPointerGraphicRaycaster>().enabled = false;
    }
    
    public void QuizStart()
    {
        Broadcast("QuizStartCommand");
    }

    [OnCommand]
    public void QuizStartCommand()
    {
        Debug.Log("작동 안하나?");
        if (timerCor != null)
        {
            StopCoroutine(timerCor);
        }
        timerCor = StartCoroutine(QuizTimerCount());
        QuizCanvas.GetComponent<Pvr_UICanvas>().enabled = true;
    }

    public void QuizEnd()
    {
        QuizAnswerCheck();
        QuizCanvas.GetComponent<Pvr_UICanvas>().enabled = false;
    }

    public void QuizAnswerCheck()
    {
        if(quizSelect[answer].isOn)
        {
            CorrentAnswer();
        }
        else
        {
            IncorrectAnswer();
        }
    }

    public void CorrentAnswer()
    {
        Debug.Log("정답입니다");
    }

    public void IncorrectAnswer()
    {
        Debug.Log("오답입니다");
    }

    IEnumerator QuizTimerCount()
    {
        for(int i = 10; i>=0; i--)
        {
            Timer.text = i.ToString();
            yield return new WaitForSeconds(1);
        }
        QuizEnd();
    }
    
    [OnCommand]
    public void SelectItem(int num)
    {

    }

}
