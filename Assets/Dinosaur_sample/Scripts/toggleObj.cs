﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class toggleObj : MonoBehaviour {

    [SerializeField]
    GameObject[] Tobjects;

	// Use this for initialization

    public void ToggleObj(int tint)
    {
        for(int i = 0; i < Tobjects.Length; i++)
        {
            Tobjects[i].SetActive(false);
        }
        Tobjects[tint].SetActive(true);
    }

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
