﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

public class CoinChance : AutoCommander
{
    public CanvasGroup canvasGroup;
    public GameObject gameEndObj;
    public GameObject countdown;
    public CoinChanceItem[] items;
    private int count;
    bool started;

    protected override void Awake()
    {
        base.Awake();
        Debug.Log("MaxUser : " + RoomInfo.main.maxUser + " count : " + (RoomInfo.main.maxUser / 10 + 1) * 10);
        count = (RoomInfo.main.maxUser / 10 + 1) * 10;
        if (count > items.Length - 1)
            count = items.Length - 1;

        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
            items[i].coin.SetActive(false);
            items[i].GetComponent<CapsuleCollider>().radius = 0.0f;
            if (items[i].effect != null)
                items[i].effect.SetActive(false);
        }
    }

    public void BroadcastGameStart()
    {
        if (!started)
        {
            started = true;
            Broadcast("GameStart");
        }
    }
    public void BroadcastCatch(CoinChanceItem item)
    {
        Broadcast("Find", PlayerInstance.main.instanceId, item.idx);
    }

    [OnCommand]
    void GameStart()
    {
        started = true;
        canvasGroup.twnAlpha(0, 0.5f).SetOnComplete(() => canvasGroup.gameObject.SetActive(false));
        countdown.SetActive(true);

        if (PlayerInstance.main.IsRoomMaster)
        {
            for (int i = 0; i < count; i++)
            {
                while (true)
                {
                    var coin = items.Random();
                    if (!coin.coin.activeSelf)
                    {
                        coin.coin.SetActive(true);

                        coin.GetComponent<CapsuleCollider>().radius = 1.5f;
                        Broadcast("SetActive", items.IndexOf(coin));
                        break;
                    }
                }
            }
        }
    }
    public void GameEnd()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].idx = i;
            items[i].coin.SetActive(false);
            items[i].GetComponent<CapsuleCollider>().radius = 0.0f;
            if (items[i].effect != null)
                items[i].effect.SetActive(false);
        }
        if (gameEndObj != null)
        {
            gameEndObj.gameObject.SetActive(true);
            SoundManager.INS.PlayEffectSound(SOUND_EFFECT_ID.ENDING_EFFECT);
        }
    }
    [OnCommand]
    void SetActive(int idx)
    {
        items[idx].coin.SetActive(true);
        items[idx].GetComponent<CapsuleCollider>().radius = 1.5f;

        if (items[idx].effect)
            items[idx].effect.SetActive(false);
    }
    [OnCommand]
    void Find(int instanceId, int idx)
    {
        if (PlayerInstance.main.IsRoomMaster && items[idx].coin.activeSelf)
        {
            while (true)
            {
                var coin = items.Random();
                if (!coin.coin.activeSelf)
                {
                    coin.coin.SetActive(true);
                    coin.GetComponent<CapsuleCollider>().radius = 1.5f;

                    Broadcast("SetActive", items.IndexOf(coin));
                    break;
                }
            }

            items[idx].coin.SetActive(false);
            items[idx].GetComponent<CapsuleCollider>().radius = 1.5f;
            Networker.Send(C_Scene.UpdateScore(instanceId, C_Scene.UpdateType.Relative, 1));
            Broadcast("Finding", idx, instanceId);
        }
    }
    [OnCommand]
    void Finding(int idx, int instanceId)
    {
        items[idx].coin.SetActive(false);
        items[idx].GetComponent<CapsuleCollider>().radius = 0.0f;
        if (items[idx].effect)
            items[idx].effect.SetActive(true);

        if (instanceId == PlayerInstance.lookMain.instanceId)
        {
            SoundManager.INS.PlayEffectSound(SOUND_EFFECT_ID.GETCOIN_EFFECT);
            var go = Instantiate(Resources.Load("CoinEffect"), items[idx].transform.position, items[idx].transform.rotation, null);
            STween.delayCall(1.1f, () => Destroy(go));
        }
    }
}
