﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowWithCoin : MonoBehaviour {
    
    private GameObject Earth;
    public GameObject board;
    int test = 0;

    private void OnEnable()
    {
        Earth = GameObject.Find("WorldMap");
        board.SetActive(false);
        StartCoroutine(ChangeChecker());
    }

    private IEnumerator ChangeChecker()
    {
        // 지구가 비활성화 될 때까지 대기
        while (true)
        {
            if (Earth.activeInHierarchy == false)
                break;
            yield return Waits.Wait(1);
        }
        board.SetActive(true);

    }
}
