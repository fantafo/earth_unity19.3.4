﻿using FTF;
using FTF.Earth;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF
{
    public class ActiveArgs
    {
        public float delay;
        public void AddDelay(float mewDeay)
        {
            delay = Mathf.Max(delay, mewDeay);
        }
    }

    /// <summary>
    /// Earth에서는 여러 국가들이 있고, 선택될 수 있습니다.
    /// 하지만 동일한 버튼이 그 역할을 수행해야하기때문에 만들어졌습니다.
    /// 이 컴포넌트의 ToggleActive()를 호출하면
    /// 
    /// 현재 선택된 나라의 index를 이용해서,
    /// 
    /// countries 자식중 index번째 자식만을 활성화시키도록 만들어져 있습니다.
    /// </summary>
    public class ActiveForCountry : AutoCommander
    {
        public Transform countries;
        public GameObject activeMark;
        public bool active;

        bool appliedActive; // 실제 적용된 active상태
        bool isNextAction; // 딜레이를 기다린 뒤 해야할 엑션이 있을경우
        float delay; // 엑션을 취한 뒤 산출된 다음 엑션까지의 딜레이
        bool nextActive;

        protected override void Awake()
        {
            base.Awake();

            active = false;

            if (activeMark)
                activeMark.SetActive(false);

            for (int i = 0, len = countries.childCount; i < len; i++)
            {
                countries.GetChild(i).gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if (PlayerInstance.main.IsRoomMaster && KeyStack.Is(this) && VRInput.BackButton)
            {
                SetActive(false);
            }

            if (isNextAction && Time.time > delay)
            {
                isNextAction = false;
                SetActive(nextActive);
            }
        }

        public void ToggleActive()
        {
            if (PlayerInstance.main.IsRoomMaster)
            {
                SetActive(!active);
            }
        }

        public void SetActive(bool active)
        {
            this.active = active;

            // 딜레이가 걸려있는 시간속에서 요청이 왔을때
            if (Time.time <= delay)
            {
                isNextAction = true;
                nextActive = active;
                return;
            }

            if (this.appliedActive != this.active)
            {
                this.appliedActive = this.active;
                Broadcast("Active", name, DemoEarthController.main.selectID, active);
            }
        }

        ActiveArgs args = new ActiveArgs();
        [OnCommand]
        void Active(string name, int index, bool active)
        {
            if (this.name == name)
            {
                if (activeMark)
                    activeMark.SetActive(active);
                args.delay = 0;

                for (int i = 0, len = countries.childCount; i < len; i++)
                {
                    countries.GetChild(i).SendMessage("Hide", args, SendMessageOptions.DontRequireReceiver);
                }

                if (active)
                {
                    if (!KeyStack.Contains(this))
                        KeyStack.Push(this);

                    countries.GetChild(index).gameObject.SetActive(true);
                    countries.GetChild(index).SendMessage("Show", args, SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    if (KeyStack.Contains(this))
                        KeyStack.Pop(this);
                }

                this.active = active;
                delay = Time.time + args.delay;
            }
        }
    }
}