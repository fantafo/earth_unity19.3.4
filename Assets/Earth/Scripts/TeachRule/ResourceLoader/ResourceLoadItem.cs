﻿using FTF.Earth;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 새로운 리소스로 이동하기위한 컴포넌트 아이템입니다.
    /// Load()가 호출되면 입력된 path의 리소스를 새로 불러옵니다.
    /// </summary>
    public class ResourceLoadItem : AutoCommander
    {
        public string path;

        public void Load()
        {
            ResourceLoader.main.Broadcast("Show", path);
        }
    }
}
