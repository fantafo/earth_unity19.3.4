﻿using FTF.Earth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace FTF
{
    /// <summary>
    /// 선생님 매뉴에 관한 이벤트를 정의합니다.
    /// 
    /// 정보보기, 카달로그, 학생 포메이션 등의 매뉴상의 엑션을 정의합니다.
    /// </summary>
    public class TeachRuleController : SMonoBehaviour
    {
        public GameObject teachingFormation;
        public GameObject roundingFormation;
        public Animator animator;
        public GameObject menuObject;
        public ActiveForCountry infoActivator;
        public ActiveForCountry catalogActivator;
        public Toggle infoBtn;
        public Toggle ctalogBtn;

        private void Awake()
        {
            infoBtn.onValueChanged.AddListener((active) =>
            {
                infoActivator.SetActive(active);
            });
            ctalogBtn.onValueChanged.AddListener((active) =>
            {
                catalogActivator.SetActive(active);
            });
        }

        private void OnDisable()
        {
            infoBtn.isOn = false;
            ctalogBtn.isOn = false;
        }

        private void Update()
        {
            if (DemoEarthController.main.isInit)
            {
                var teachAct = teachingFormation.activeSelf;
                var roundAct = roundingFormation.activeSelf;

                // 라운드 혹은 가르침 포메이션일때만 켜진다.
                if (teachAct || roundAct)
                {
                    // 국가 설명 버튼은 티칭모드일 경우에만 켜진다.
                    infoBtn.interactable = teachAct;

                    infoBtn.isOn = infoActivator.active;
                    ctalogBtn.isOn = catalogActivator.active;

                    // 메뉴가 꺼져있다면 메뉴를 킨다.
                    if (!menuObject.activeSelf)
                    {
                        animator.SetBool("Open", true);
                    }
                }
                else
                {
                    // 메뉴가 켜져있다면 끈다
                    if (menuObject.activeSelf)
                    {
                        animator.SetBool("Open", false);
                    }
                }
            }
        }
    }
}
