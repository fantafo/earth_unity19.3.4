﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class VideoScript : MonoBehaviour {
    public string VideoPath;
    private GameObject VideoPlayer;

    private void OnEnable()
    {
        VideoPlayer = GameObject.Find("VideoPlayerObject");
        VideoPlayer.GetComponent<MeshRenderer>().enabled = true;
        Debug.Log("MeshRenderer Enabled");
        VideoPlayer.GetComponent<MediaPlayer>().OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, VideoPath, true);
    }


    private void OnDestroy()
    {
        VideoPlayer.GetComponent<MeshRenderer>().enabled = false;
        VideoPlayer.GetComponent<MediaPlayer>().Stop();
    }

}
