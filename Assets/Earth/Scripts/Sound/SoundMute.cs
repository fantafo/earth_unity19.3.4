﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FTF;
using FTF.Packet;

public class SoundMute : AutoCommander
{
	public void MuteClick ()
    {
        Broadcast("MuteSound");
    }

    [OnCommand]
    void MuteSound ()
    {
        SoundManager.INS.PlayBGSound(SOUND_BG_ID.NONE);
    }
}
