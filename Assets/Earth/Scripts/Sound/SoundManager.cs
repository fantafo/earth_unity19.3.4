﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SOUND_BG_ID
{
    MAIN_BG,
    EZYPT_BG,
    JURASIC_BG,
    PARI_BG,
    NONE,
}

public enum SOUND_EFFECT_ID
{
    GETCOIN_EFFECT,
    WRONG_EFFECT,
    CORRECT_EFFECT,
    TREX_EFFECT,
    KARNO_EFFECT,
    COMPI_EFFECT,
    ENDING_EFFECT,
}


public class SoundManager : MonoBehaviour {

    public static SoundManager INS;
    private void Awake()
    {
        INS = this;
    }

    public List<AudioSource> m_BGs = new List<AudioSource>();
    public List<AudioSource> m_Effects = new List<AudioSource>();

    private void Start()
    {
        PlayBGSound(SOUND_BG_ID.MAIN_BG);
    }

    private void Update()
    {
        if (GameObject.FindWithTag("map") != null && m_CurrentBG.isPlaying)
        {
            //m_CurrentBG.mute = true;
            m_CurrentBG.Stop();
        }
        else if(!m_CurrentBG.isPlaying)
        {
            //m_CurrentBG.mute = false;
            m_CurrentBG.Play();
        }
    }

    AudioSource m_CurrentBG;
    public void PlayBGSound (SOUND_BG_ID _id)
    {
        AudioSource a_source = m_BGs[(int)_id];
        if (a_source != m_CurrentBG)
        {
            if (m_CurrentBG != null)
            {
                m_CurrentBG.Stop();
            }

            m_CurrentBG = a_source;
            m_CurrentBG.Play();
        }
    }

    public void PlayEffectSound (SOUND_EFFECT_ID _id)
    {
        m_Effects[(int)_id].Play();
    }
	
}
