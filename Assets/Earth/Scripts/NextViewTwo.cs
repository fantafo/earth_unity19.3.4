﻿using FTF;
using FTF.Earth;
using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class NextViewTwo : MonoBehaviour {
	//next, nextVideo중 하나는 반드시 null
	public ModelViewer[] next;
    
	private int index = 0;
	private bool view = false;

	public bool nextViewDelete = true;


	public string nextVideo = "";

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER) || Input.GetKeyDown("space"))
		{
			
			if (nextViewDelete)
			{
				if (!view)
				{
					next[index].Toggle();
					view = true;
				}
				else
				{
					next[index].Broadcast("SetActive", next[index].name, false);
					index++;
					view = false;
				}
			}
			else
			{
				next[index].Toggle();
				index++;
			}

			if (!nextVideo.IsEmpty()&&index >= next.Length)
			{
				ResourceLoader.main.Broadcast("Show", nextVideo);
			}
		}

	}
}
