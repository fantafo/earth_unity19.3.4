﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FTF.Earth
{
    [Obsolete]
    public interface ILongRaycastTrigger
    {
        void Click();
    }
}