﻿using FTF;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Unity VideoPlayer에 문제가 있었기에, 현재 사용하지 않습니다.
/// </summary>
public class MoviePlayer : SMonoBehaviour
{
    public string path;
    private void Start()
    {
        var player = GetComponent<VideoPlayer>();
        string url = string.Empty;

#if (UNITY_EDITOR || UNITY_STANDALONE_WIN)
        url = "file:///" + Application.streamingAssetsPath + "/" + path;
#elif UNITY_ANDROID
        url =  "jar:file://" + Application.dataPath + "!/assets/" + path;
#elif UNITY_IOS
        url = "file:///" + Application.streamingAssetsPath + "/"+path;
#endif

        player.url = url;
        player.Play();
    }
}
